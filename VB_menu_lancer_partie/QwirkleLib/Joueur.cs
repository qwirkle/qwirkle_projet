﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QwirkleLib
{
    public class Joueur
    {


        private string name;
        private int score;
        private int age;
        private Tuile[] main_joueur;

        public Joueur(string name, int score, int age) //Constructeur (à tester)
        {
            this.name = name;
            this.score = score;
            this.age = age;
        }

        public string GetName()
        {
            return this.name;
        }
        public int GetScore()
        {
            return this.score;
        }
        public Tuile[] GetMain_joueur()
        {
            return this.main_joueur;
        }
        public int GetAge()
        {
            return this.age;
        }

        public void SetName(string name)
        {
            this.name = name;
        }
        public void SetScore(int score)
        {
            this.score = score;
        }
        public void SetMain_joueur(Tuile[] main_joueur)
        {
            this.main_joueur = main_joueur;
        }
        public void SetAge(int age)
        {
            this.age = age;
        }

        public void PlacerTuiles(Tuile TuileAPlacer, int coord_xVB, int coord_yVB)
        {
           
            Tuile TuileIntermediaire = TuileAPlacer;

            for (int IndiceMainJoueur = 0; IndiceMainJoueur < 6; IndiceMainJoueur++)
            {
                if (main_joueur[IndiceMainJoueur] == TuileAPlacer)
                {
                    main_joueur[IndiceMainJoueur] = null;
                 
                    break;
                    
                }
               
            }

            Plateau.PlacementTuile(TuileIntermediaire, coord_xVB, coord_yVB);
    
        }
       
        public void EchangerTuiles(Tuile TuileAEchanger, Pioche pioche)
        {
            Tuile TuileTampon = TuileAEchanger;
            List<Tuile> recupPioche = pioche.GetTuilesRestantes();

            if (pioche.GetTuilesRestantes().Count >= 1)
            {
                for (int IndiceMainJoueur = 0; IndiceMainJoueur < 6; IndiceMainJoueur++)
                {
                    if (main_joueur[IndiceMainJoueur] == TuileAEchanger)
                    {
                        main_joueur[IndiceMainJoueur] = null;
                        break;
                    }
                }
                main_joueur = pioche.DistributionTuiles(main_joueur);
                recupPioche.Add(TuileTampon);
                pioche.SetTuilesRestantes(recupPioche);
            }
        }
        public int ActualiserPoint(int coord_xVB, int coord_yVB) //Régler problème guillaume
        {
            Case[,] plateau = Plateau.GetPlateau_Jeu();
            int comptagegauche = 0, comptagedroite = 0, comptagehaut = 0, comptagebas = 0;
            int scorefinal = 0;

           for (int increment_X = coord_xVB; plateau[increment_X-1, coord_yVB].GetContenu() != null; increment_X--)// Vérification vers la gauche
                {
                if (plateau[increment_X-1,coord_yVB].GetContenu() != null)
                {
                    if (comptagegauche == 0)
                        comptagegauche++;
                    comptagegauche++;
                }
            }

            for (int increment_X = coord_xVB; plateau[increment_X+1, coord_yVB].GetContenu() != null; increment_X++)// Vérification vers la droite
                {
                if (plateau[increment_X+1,coord_yVB ].GetContenu() != null)
                {
                    if (comptagedroite == 0)
                        comptagedroite++;
                    comptagedroite++;
                }
            }

            for (int increment_Y = coord_yVB; plateau[coord_xVB, increment_Y+1].GetContenu() != null; increment_Y++)// Vérification vers le haut
            {
                if (plateau[coord_xVB,increment_Y+1].GetContenu() != null)
                {
                    if (comptagehaut == 0)
                        comptagehaut++;
                    comptagehaut++;
                }
            }

            for (int increment_Y = coord_yVB; plateau[coord_xVB, increment_Y-1].GetContenu() != null; increment_Y--)// Vérification vers le bas
            {
                if (plateau[coord_xVB,increment_Y-1].GetContenu() != null)
                {
                    if (comptagebas == 0)
                        comptagebas++;
                    comptagebas++;
                }
            }

            if (comptagebas == 6)
            {
                comptagebas = comptagebas + 6;
            }
            else if (comptagehaut == 6)
            {
                comptagehaut = comptagehaut + 6;
            }
            else if (comptagedroite == 6)
            {
                comptagedroite = comptagedroite + 6;
            }
            else if (comptagegauche == 6)
            {
                comptagegauche = comptagegauche + 6;
            }

            scorefinal = comptagebas + comptagehaut + comptagedroite + comptagegauche;
            
            return scorefinal;
            
        }

        public int DefinirPremierJoueur(Tuile[] main_joueur) // ajouter l'age
        {
            int NbMaxFormeCommun = 0;
            int NbMaxCouleurCommun = 0;
            int PointTotal = 0;
            int Comptage_Couleur = 0;
            int Comptage_Forme = 0;
            List<Tuile> ComptageForme = new List<Tuile>();
            List<Tuile> ComptageCouleur = new List<Tuile>();
            int[] TableauVerifPassageForme = { 0, 0, 0, 0, 0, 0 };
            int[] TableauVerifPassageCouleur = { 0, 0, 0, 0, 0, 0 };

            for (int IndiceMainJoueur = 0; IndiceMainJoueur < 6; IndiceMainJoueur++)
            {
                for (int IndiceMainParcours = 0; IndiceMainParcours < 6; IndiceMainParcours++)
                {
                    if (main_joueur[IndiceMainParcours].GetForme() == main_joueur[IndiceMainJoueur].GetForme() && main_joueur[IndiceMainParcours].GetCouleur() != main_joueur[IndiceMainJoueur].GetCouleur())
                    {
                        TableauVerifPassageForme[IndiceMainParcours] = 1;
                        ComptageForme.Add(main_joueur[IndiceMainParcours]);

                        if (TableauVerifPassageForme[IndiceMainJoueur] != 1)
                        {
                            ComptageForme.Add(main_joueur[IndiceMainJoueur]);
                            TableauVerifPassageForme[IndiceMainJoueur] = 1;
                        }
                    }

                    if (main_joueur[IndiceMainParcours].GetForme() != main_joueur[IndiceMainJoueur].GetForme() && main_joueur[IndiceMainParcours].GetCouleur() == main_joueur[IndiceMainJoueur].GetCouleur())
                    {
                        TableauVerifPassageCouleur[IndiceMainParcours] = 1;
                        ComptageCouleur.Add(main_joueur[IndiceMainParcours]);

                        if (TableauVerifPassageCouleur[IndiceMainJoueur] != 1)
                        {
                            ComptageCouleur.Add(main_joueur[IndiceMainJoueur]);
                            TableauVerifPassageCouleur[IndiceMainJoueur] = 1;
                        }
                    }
                }

                Comptage_Forme = ComptageForme.Count;
                ComptageForme.Clear();
                Comptage_Couleur = ComptageCouleur.Count;
                ComptageCouleur.Clear();

                if (NbMaxFormeCommun <= Comptage_Forme)
                {
                    NbMaxFormeCommun = Comptage_Forme;
                }
                if (NbMaxCouleurCommun <= Comptage_Couleur)
                {
                    NbMaxCouleurCommun = Comptage_Couleur;
                }
            }

            if (NbMaxFormeCommun <= NbMaxCouleurCommun)
            {
                PointTotal = NbMaxCouleurCommun;
            }
            else
            {
                PointTotal = NbMaxFormeCommun;
            }
            return PointTotal;
        }

    }

}