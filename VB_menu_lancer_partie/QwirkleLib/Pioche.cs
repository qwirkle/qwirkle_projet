﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QwirkleLib
{
    public class Pioche
    {
        private List<Tuile> tuiles_restantes;

        public Pioche() //Constructeur (à tester)
        {
            this.tuiles_restantes = Tuile.CreationTuiles();
        }

        public List<Tuile> GetTuilesRestantes()
        {
            return this.tuiles_restantes;
        }

        public void SetTuilesRestantes(List<Tuile> tuiles_restantes)
        {
            this.tuiles_restantes = tuiles_restantes;
        }

        public Tuile[] FirstDistributionTuiles()
        {
            Tuile[] main_joueur = new Tuile[6];
            Random random = new Random();
            int TuilesRandom;
            int taillePioche = tuiles_restantes.Count;

            for (int IndiceMainJoueur = 0; IndiceMainJoueur < 6; IndiceMainJoueur++)
            {
                TuilesRandom = random.Next(0, taillePioche); //Selection d'une valeur (NEXT)
                main_joueur[IndiceMainJoueur] = tuiles_restantes[TuilesRandom];
                tuiles_restantes.Remove(tuiles_restantes[TuilesRandom]); //Supprime la tuile prise dans la pioche
            }
            return main_joueur;
        }
        public Tuile[] DistributionTuiles(Tuile[] main_joueur)//Dans le main, le mettre dans une boucle avec n=nbTuileManquante
        {
            Random random = new Random();
            int TuilesRandom;
            int taillePioche = tuiles_restantes.Count;
            int IndiceMainJoueur;

            for (IndiceMainJoueur = 0; IndiceMainJoueur < 6; IndiceMainJoueur++)
            {
                if ((main_joueur[IndiceMainJoueur] is null) && (taillePioche != 0))
                {
                    TuilesRandom = random.Next(0, taillePioche);
                    main_joueur[IndiceMainJoueur] = tuiles_restantes[TuilesRandom];
                    tuiles_restantes.Remove(tuiles_restantes[TuilesRandom]);
                }
            }
            return main_joueur;
        }
    }
}


