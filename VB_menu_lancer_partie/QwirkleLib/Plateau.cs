﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleLib
{
    public  class Plateau
    {
        private static Case[,] plateau_jeu;

        static Plateau() //Constructeur (à tester)
        {
            plateau_jeu = new Case[30, 30];

            for (int IndiceX = 0; IndiceX < 30; IndiceX++)
            {
                for (int IndiceY = 0; IndiceY < 30; IndiceY++)
                {
                    plateau_jeu[IndiceX, IndiceY] = new Case(IndiceX, IndiceY);
                }
            }
        }

        public static Case[,] GetPlateau_Jeu()
        {
            return plateau_jeu;
        }

        public static void PlacementTuile(Tuile tuile, int coord_x, int coord_y)
        {
            plateau_jeu[coord_x, coord_y].SetContenu(tuile);
        } 
    }
}
