﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace QwirkleLib
{
    public class Tuile
    {
        private string couleur;
        private string forme;

        public Tuile(string forme, string couleur)
        {
            this.forme = forme;
            this.couleur = couleur;
        }

        public string GetCouleur() { return this.couleur; }
        public string GetForme() { return this.forme; }

        public void SetCouleur(string couleur) { this.couleur = couleur; }
        public void SetForme(string forme) { this.forme = forme; }

        public static List<Tuile> CreationTuiles()
        {
            string[] TabForme = { "Rond", "Trefle", "Croix", "Losange", "Carre", "Etoile" };
            string[] TabCouleur = { "Rouge", "Bleu", "Vert", "Jaune", "Orange", "Violet" };
            List<Tuile> tuiles = new List<Tuile>();

            for (int IndiceForme = 0; IndiceForme < 6; IndiceForme++)
            {
                for (int IndiceCouleur = 0; IndiceCouleur < 6; IndiceCouleur++)
                {
                    for (int IndiceNbTuileRepete = 0; IndiceNbTuileRepete < 3; IndiceNbTuileRepete++)
                    {
                        tuiles.Add(new Tuile(TabForme[IndiceForme], TabCouleur[IndiceCouleur]));
                    }
                }
            }
            tuiles = tuiles.OrderBy(x => new Random().Next()).ToList(); // Rangement aléatoire
            return tuiles;
        }
    }
}

