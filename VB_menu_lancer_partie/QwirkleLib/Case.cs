﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleLib
{
    public class Case
    {
        private Tuile contenu;
        private int coord_x;
        private int coord_y;


        public Case(int coord_x, int coord_y) //Constructeur (à tester)
        {
            this.coord_x = coord_x;
            this.coord_y = coord_y;
        }

        public Tuile GetContenu()
        {
            return this.contenu;
        }
        public int GetCoord_x()
        {
            return this.coord_x;
        }
        public int GetCoord_y()
        {
            return this.coord_y;
        }

        public void SetContenu(Tuile contenu)
        {
            this.contenu = contenu;
        }
        public void SetCoord_x(int coord_x)
        {
            this.coord_x = coord_x;
        }
        public void SetCoord_y(int coord_y)
        {
            this.coord_y = coord_y;
        }

        public bool VerifierCase(int coord_xVB, int coord_yVB)
        {
            bool confirmation;
            Case[,] plateau = Plateau.GetPlateau_Jeu();

            if (plateau[coord_xVB, coord_yVB].GetContenu() is null)
            {
                confirmation = true;
            }
            else
            {
                confirmation = false;
            }

            return confirmation;
        }
        public bool VerifierCombi(int CompteurTour, int coord_xVB, int coord_yVB)
        {
            Case[,] plateau = Plateau.GetPlateau_Jeu();
            bool placementPossible;
            bool placementPossibleXDroite = false;
            bool placementPossibleXGauche = false;
            bool placementPossibleYBas = false;
            bool placementPossibleYHaut = false;

            if (CompteurTour != 0)
            {
                for (coord_x = coord_xVB; (plateau[coord_x, coord_yVB].GetContenu() != null && plateau[coord_x + 1, coord_yVB].GetContenu() != null); coord_x++) // Vérification vers la droite
                {
                    if (((plateau[coord_x, coord_yVB].GetContenu().GetForme() == plateau[coord_x + 1, coord_yVB].GetContenu().GetForme()) && (plateau[coord_x, coord_yVB].GetContenu().GetCouleur() != plateau[coord_x + 1, coord_yVB].GetContenu().GetCouleur())))
                    {
                        placementPossibleXDroite = true;
                    }
                    else
                    {
                        placementPossibleXDroite = false;
                    }

                    if ((plateau[coord_x, coord_yVB].GetContenu().GetCouleur() == plateau[coord_x + 1, coord_yVB].GetContenu().GetCouleur()) && (plateau[coord_x, coord_yVB].GetContenu().GetForme() != plateau[coord_x + 1, coord_yVB].GetContenu().GetForme()))
                    {
                        placementPossibleXDroite = true;
                    }
                    else
                    {
                        placementPossibleXDroite = false;
                    }

                    if (placementPossibleXDroite == false)
                        break;
                }
                for (coord_x = coord_xVB; (plateau[coord_x, coord_yVB].GetContenu() != null && plateau[coord_x - 1, coord_yVB].GetContenu() != null); coord_x--) // Vérification vers la gauche
                {
                    if (((plateau[coord_x, coord_yVB].GetContenu().GetForme() == plateau[coord_x - 1, coord_yVB].GetContenu().GetForme()) && (plateau[coord_x, coord_yVB].GetContenu().GetCouleur() != plateau[coord_x - 1, coord_yVB].GetContenu().GetCouleur())))
                    {
                        placementPossibleXGauche = true;
                    }
                    else
                    {
                        placementPossibleXGauche = false;
                    }

                    if ((plateau[coord_x, coord_yVB].GetContenu().GetCouleur() == plateau[coord_x - 1, coord_yVB].GetContenu().GetCouleur()) && (plateau[coord_x, coord_yVB].GetContenu().GetForme() != plateau[coord_x - 1, coord_yVB].GetContenu().GetForme()))
                    {
                        placementPossibleXGauche = true;
                    }
                    else
                    {
                        placementPossibleXGauche = false;
                    }

                    if (placementPossibleXGauche == false)
                        break;
                }

                for (coord_y = coord_yVB; (plateau[coord_xVB, coord_y].GetContenu() != null && plateau[coord_xVB, coord_y + 1].GetContenu() != null); coord_y++) // Vérification vers le bas
                {
                    if ((plateau[coord_xVB, coord_y].GetContenu().GetForme() == plateau[coord_xVB, coord_y + 1].GetContenu().GetForme()) && (plateau[coord_xVB, coord_y].GetContenu().GetCouleur() != plateau[coord_xVB, coord_y + 1].GetContenu().GetCouleur()))
                    {
                        placementPossibleYBas = true;
                    }
                    else
                    {
                        placementPossibleYBas = false;
                        break;
                    }

                    if (((plateau[coord_xVB, coord_y].GetContenu().GetCouleur() == plateau[coord_xVB, coord_y + 1].GetContenu().GetCouleur()) && (plateau[coord_xVB, coord_y].GetContenu().GetForme() != plateau[coord_xVB, coord_y + 1].GetContenu().GetForme())))
                    {
                        placementPossibleYBas = true;
                    }
                    else
                    {
                        placementPossibleYBas = false;
                    }

                    if (placementPossibleYBas == false)
                        break;
                }
                for (coord_y = coord_yVB; (plateau[coord_xVB, coord_y].GetContenu() != null && plateau[coord_xVB, coord_y - 1].GetContenu() != null); coord_y--) // Vérification vers le haut
                {
                    if (((plateau[coord_xVB, coord_y].GetContenu().GetForme() == plateau[coord_xVB, coord_y - 1].GetContenu().GetForme()) && (plateau[coord_xVB, coord_y].GetContenu().GetCouleur() != plateau[coord_xVB, coord_y - 1].GetContenu().GetCouleur())))
                    {
                        placementPossibleYHaut = true;
                    }
                    else
                    {
                        placementPossibleYHaut = false;
                    }

                    if (((plateau[coord_xVB, coord_y].GetContenu().GetCouleur() == plateau[coord_xVB, coord_y - 1].GetContenu().GetCouleur()) && (plateau[coord_xVB, coord_y].GetContenu().GetForme() != plateau[coord_xVB, coord_y - 1].GetContenu().GetForme())))
                    {
                        placementPossibleYHaut = true;
                    }
                    else
                    {
                        placementPossibleYHaut = false;
                    }

                    if (placementPossibleYHaut == false)
                        break;
                }
            }
            else
            {
                placementPossibleXDroite = true;
                placementPossibleXGauche = true;
                placementPossibleYBas = true;
                placementPossibleYHaut = true;
            }


            if ((placementPossibleXDroite == true) && (placementPossibleXGauche == true) && (placementPossibleYBas == true) && (placementPossibleYHaut == true))
            {
                placementPossible = true;
            }
            else
            {
                placementPossible = false;
            }

            return placementPossible;
        }
    }
}

