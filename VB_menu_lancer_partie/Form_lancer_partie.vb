﻿Imports QwirkleLib
Public Class Frm_lancer_partie


    Private Sub rdb_4_joueurs_CheckedChanged(sender As Object, e As EventArgs) Handles rdb_4_joueurs.CheckedChanged
        txt_j3.Enabled = True
        txt_j4.Enabled = True

    End Sub

    Private Sub rdb_3_joueurs_CheckedChanged(sender As Object, e As EventArgs) Handles rdb_3_joueurs.CheckedChanged
        txt_j3.Enabled = True
        txt_j4.Enabled = False
    End Sub

    Private Sub rdb_2_joueurs_CheckedChanged(sender As Object, e As EventArgs) Handles rdb_2_joueurs.CheckedChanged
        txt_j3.Enabled = False
        txt_j4.Enabled = False
    End Sub

    Private Sub cmd_jouer_Click(sender As Object, e As EventArgs) Handles cmd_jouer.Click
        Dim Plateau As New Form
        Plateau = frm_tableau
        Plateau.ShowDialog()

        Me.Close()
    End Sub

End Class
