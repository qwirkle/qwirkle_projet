﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_tableau
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lbl_pts_j1 = New System.Windows.Forms.Label()
        Me.lbl_pts_j3 = New System.Windows.Forms.Label()
        Me.lbl_pts_j2 = New System.Windows.Forms.Label()
        Me.cmd_echanger = New System.Windows.Forms.Button()
        Me.cmd_quitter = New System.Windows.Forms.Button()
        Me.cmd_passer = New System.Windows.Forms.Button()
        Me.cmd_valider = New System.Windows.Forms.Button()
        Me.cmd_aide = New System.Windows.Forms.Button()
        Me.lbl_points1 = New System.Windows.Forms.Label()
        Me.lbl_points3 = New System.Windows.Forms.Label()
        Me.lbl_points2 = New System.Windows.Forms.Label()
        Me.grp_box_pieces_j1 = New System.Windows.Forms.GroupBox()
        Me.pic_piece_12 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_13 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_14 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_15 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_16 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_11 = New System.Windows.Forms.PictureBox()
        Me.grp_box_nom_j1 = New System.Windows.Forms.GroupBox()
        Me.lbl_score1 = New System.Windows.Forms.Label()
        Me.grp_box_nom_j2 = New System.Windows.Forms.GroupBox()
        Me.lbl_score2 = New System.Windows.Forms.Label()
        Me.grp_box_nom_j3 = New System.Windows.Forms.GroupBox()
        Me.lbl_score3 = New System.Windows.Forms.Label()
        Me.grp_box_pieces_j2 = New System.Windows.Forms.GroupBox()
        Me.pic_piece_22 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_23 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_24 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_25 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_26 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_21 = New System.Windows.Forms.PictureBox()
        Me.grp_box_pieces_j3 = New System.Windows.Forms.GroupBox()
        Me.pic_piece_32 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_33 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_34 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_35 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_36 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_31 = New System.Windows.Forms.PictureBox()
        Me.tlt_aide_plateau = New System.Windows.Forms.ToolTip(Me.components)
        Me.grp_box_J1 = New System.Windows.Forms.GroupBox()
        Me.grp_box_J2 = New System.Windows.Forms.GroupBox()
        Me.grp_box_J3 = New System.Windows.Forms.GroupBox()
        Me.grp_box_nom_j4 = New System.Windows.Forms.GroupBox()
        Me.lbl_score4 = New System.Windows.Forms.Label()
        Me.lbl_points4 = New System.Windows.Forms.Label()
        Me.lbl_pts_j4 = New System.Windows.Forms.Label()
        Me.grp_box_pieces_j4 = New System.Windows.Forms.GroupBox()
        Me.pic_piece_42 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_43 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_44 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_45 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_46 = New System.Windows.Forms.PictureBox()
        Me.pic_piece_41 = New System.Windows.Forms.PictureBox()
        Me.grp_box_J4 = New System.Windows.Forms.GroupBox()
        Me.lbl_test = New System.Windows.Forms.Label()
        Me.grp_box_pieces_j1.SuspendLayout()
        CType(Me.pic_piece_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_box_nom_j1.SuspendLayout()
        Me.grp_box_nom_j2.SuspendLayout()
        Me.grp_box_nom_j3.SuspendLayout()
        Me.grp_box_pieces_j2.SuspendLayout()
        CType(Me.pic_piece_22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_box_pieces_j3.SuspendLayout()
        CType(Me.pic_piece_32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_31, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_box_J1.SuspendLayout()
        Me.grp_box_J2.SuspendLayout()
        Me.grp_box_J3.SuspendLayout()
        Me.grp_box_nom_j4.SuspendLayout()
        Me.grp_box_pieces_j4.SuspendLayout()
        CType(Me.pic_piece_42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_piece_41, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_box_J4.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_pts_j1
        '
        Me.lbl_pts_j1.AutoSize = True
        Me.lbl_pts_j1.Location = New System.Drawing.Point(166, 20)
        Me.lbl_pts_j1.Name = "lbl_pts_j1"
        Me.lbl_pts_j1.Size = New System.Drawing.Size(55, 18)
        Me.lbl_pts_j1.TabIndex = 6
        Me.lbl_pts_j1.Text = "pts_j1"
        '
        'lbl_pts_j3
        '
        Me.lbl_pts_j3.AutoSize = True
        Me.lbl_pts_j3.Location = New System.Drawing.Point(166, 19)
        Me.lbl_pts_j3.Name = "lbl_pts_j3"
        Me.lbl_pts_j3.Size = New System.Drawing.Size(59, 18)
        Me.lbl_pts_j3.TabIndex = 8
        Me.lbl_pts_j3.Text = "pts_j3"
        '
        'lbl_pts_j2
        '
        Me.lbl_pts_j2.AutoSize = True
        Me.lbl_pts_j2.Location = New System.Drawing.Point(164, 20)
        Me.lbl_pts_j2.Name = "lbl_pts_j2"
        Me.lbl_pts_j2.Size = New System.Drawing.Size(59, 18)
        Me.lbl_pts_j2.TabIndex = 9
        Me.lbl_pts_j2.Text = "pts_j2"
        '
        'cmd_echanger
        '
        Me.cmd_echanger.Font = New System.Drawing.Font("Snap ITC", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_echanger.Location = New System.Drawing.Point(716, 848)
        Me.cmd_echanger.Name = "cmd_echanger"
        Me.cmd_echanger.Size = New System.Drawing.Size(211, 78)
        Me.cmd_echanger.TabIndex = 18
        Me.cmd_echanger.Text = "Echanger"
        Me.cmd_echanger.UseVisualStyleBackColor = True
        '
        'cmd_quitter
        '
        Me.cmd_quitter.Font = New System.Drawing.Font("Snap ITC", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_quitter.Location = New System.Drawing.Point(148, 848)
        Me.cmd_quitter.Name = "cmd_quitter"
        Me.cmd_quitter.Size = New System.Drawing.Size(205, 78)
        Me.cmd_quitter.TabIndex = 19
        Me.cmd_quitter.Text = "Quitter"
        Me.cmd_quitter.UseVisualStyleBackColor = True
        '
        'cmd_passer
        '
        Me.cmd_passer.Font = New System.Drawing.Font("Snap ITC", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_passer.Location = New System.Drawing.Point(438, 848)
        Me.cmd_passer.Name = "cmd_passer"
        Me.cmd_passer.Size = New System.Drawing.Size(208, 78)
        Me.cmd_passer.TabIndex = 20
        Me.cmd_passer.Text = "Passer"
        Me.cmd_passer.UseVisualStyleBackColor = True
        '
        'cmd_valider
        '
        Me.cmd_valider.Font = New System.Drawing.Font("Snap ITC", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_valider.Location = New System.Drawing.Point(991, 773)
        Me.cmd_valider.Name = "cmd_valider"
        Me.cmd_valider.Size = New System.Drawing.Size(205, 153)
        Me.cmd_valider.TabIndex = 21
        Me.cmd_valider.Text = "Valider"
        Me.cmd_valider.UseVisualStyleBackColor = True
        '
        'cmd_aide
        '
        Me.cmd_aide.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_aide.Location = New System.Drawing.Point(12, 879)
        Me.cmd_aide.Name = "cmd_aide"
        Me.cmd_aide.Size = New System.Drawing.Size(87, 47)
        Me.cmd_aide.TabIndex = 23
        Me.cmd_aide.Text = "Aide"
        Me.tlt_aide_plateau.SetToolTip(Me.cmd_aide, "Cliquez sur le bouton ""aide"" pour être redirigé sur les règles du Qwirkle")
        Me.cmd_aide.UseVisualStyleBackColor = True
        '
        'lbl_points1
        '
        Me.lbl_points1.AutoSize = True
        Me.lbl_points1.Location = New System.Drawing.Point(239, 20)
        Me.lbl_points1.Name = "lbl_points1"
        Me.lbl_points1.Size = New System.Drawing.Size(58, 18)
        Me.lbl_points1.TabIndex = 24
        Me.lbl_points1.Text = "points"
        '
        'lbl_points3
        '
        Me.lbl_points3.AutoSize = True
        Me.lbl_points3.Location = New System.Drawing.Point(239, 19)
        Me.lbl_points3.Name = "lbl_points3"
        Me.lbl_points3.Size = New System.Drawing.Size(58, 18)
        Me.lbl_points3.TabIndex = 26
        Me.lbl_points3.Text = "points"
        '
        'lbl_points2
        '
        Me.lbl_points2.AutoSize = True
        Me.lbl_points2.Location = New System.Drawing.Point(237, 20)
        Me.lbl_points2.Name = "lbl_points2"
        Me.lbl_points2.Size = New System.Drawing.Size(58, 18)
        Me.lbl_points2.TabIndex = 27
        Me.lbl_points2.Text = "points"
        '
        'grp_box_pieces_j1
        '
        Me.grp_box_pieces_j1.Controls.Add(Me.pic_piece_12)
        Me.grp_box_pieces_j1.Controls.Add(Me.pic_piece_13)
        Me.grp_box_pieces_j1.Controls.Add(Me.pic_piece_14)
        Me.grp_box_pieces_j1.Controls.Add(Me.pic_piece_15)
        Me.grp_box_pieces_j1.Controls.Add(Me.pic_piece_16)
        Me.grp_box_pieces_j1.Controls.Add(Me.pic_piece_11)
        Me.grp_box_pieces_j1.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_pieces_j1.Location = New System.Drawing.Point(15, 85)
        Me.grp_box_pieces_j1.Name = "grp_box_pieces_j1"
        Me.grp_box_pieces_j1.Size = New System.Drawing.Size(393, 81)
        Me.grp_box_pieces_j1.TabIndex = 28
        Me.grp_box_pieces_j1.TabStop = False
        Me.grp_box_pieces_j1.Text = "Pièces "
        '
        'pic_piece_12
        '
        Me.pic_piece_12.Location = New System.Drawing.Point(83, 18)
        Me.pic_piece_12.Name = "pic_piece_12"
        Me.pic_piece_12.Size = New System.Drawing.Size(53, 50)
        Me.pic_piece_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_12.TabIndex = 15
        Me.pic_piece_12.TabStop = False
        '
        'pic_piece_13
        '
        Me.pic_piece_13.Location = New System.Drawing.Point(144, 18)
        Me.pic_piece_13.Name = "pic_piece_13"
        Me.pic_piece_13.Size = New System.Drawing.Size(48, 50)
        Me.pic_piece_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_13.TabIndex = 14
        Me.pic_piece_13.TabStop = False
        '
        'pic_piece_14
        '
        Me.pic_piece_14.Location = New System.Drawing.Point(198, 18)
        Me.pic_piece_14.Name = "pic_piece_14"
        Me.pic_piece_14.Size = New System.Drawing.Size(47, 50)
        Me.pic_piece_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_14.TabIndex = 13
        Me.pic_piece_14.TabStop = False
        '
        'pic_piece_15
        '
        Me.pic_piece_15.Location = New System.Drawing.Point(251, 18)
        Me.pic_piece_15.Name = "pic_piece_15"
        Me.pic_piece_15.Size = New System.Drawing.Size(53, 50)
        Me.pic_piece_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_15.TabIndex = 12
        Me.pic_piece_15.TabStop = False
        '
        'pic_piece_16
        '
        Me.pic_piece_16.Location = New System.Drawing.Point(310, 18)
        Me.pic_piece_16.Name = "pic_piece_16"
        Me.pic_piece_16.Size = New System.Drawing.Size(52, 50)
        Me.pic_piece_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_16.TabIndex = 11
        Me.pic_piece_16.TabStop = False
        '
        'pic_piece_11
        '
        Me.pic_piece_11.Location = New System.Drawing.Point(28, 18)
        Me.pic_piece_11.Name = "pic_piece_11"
        Me.pic_piece_11.Size = New System.Drawing.Size(49, 50)
        Me.pic_piece_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_11.TabIndex = 10
        Me.pic_piece_11.TabStop = False
        '
        'grp_box_nom_j1
        '
        Me.grp_box_nom_j1.Controls.Add(Me.lbl_score1)
        Me.grp_box_nom_j1.Controls.Add(Me.lbl_points1)
        Me.grp_box_nom_j1.Controls.Add(Me.lbl_pts_j1)
        Me.grp_box_nom_j1.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_nom_j1.Location = New System.Drawing.Point(15, 25)
        Me.grp_box_nom_j1.Name = "grp_box_nom_j1"
        Me.grp_box_nom_j1.Size = New System.Drawing.Size(393, 54)
        Me.grp_box_nom_j1.TabIndex = 30
        Me.grp_box_nom_j1.TabStop = False
        Me.grp_box_nom_j1.Text = "Nom 1"
        '
        'lbl_score1
        '
        Me.lbl_score1.AutoSize = True
        Me.lbl_score1.Location = New System.Drawing.Point(95, 20)
        Me.lbl_score1.Name = "lbl_score1"
        Me.lbl_score1.Size = New System.Drawing.Size(65, 18)
        Me.lbl_score1.TabIndex = 25
        Me.lbl_score1.Text = "Score :"
        '
        'grp_box_nom_j2
        '
        Me.grp_box_nom_j2.Controls.Add(Me.lbl_score2)
        Me.grp_box_nom_j2.Controls.Add(Me.lbl_points2)
        Me.grp_box_nom_j2.Controls.Add(Me.lbl_pts_j2)
        Me.grp_box_nom_j2.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_nom_j2.Location = New System.Drawing.Point(14, 18)
        Me.grp_box_nom_j2.Name = "grp_box_nom_j2"
        Me.grp_box_nom_j2.Size = New System.Drawing.Size(393, 51)
        Me.grp_box_nom_j2.TabIndex = 31
        Me.grp_box_nom_j2.TabStop = False
        Me.grp_box_nom_j2.Text = "Nom 2"
        '
        'lbl_score2
        '
        Me.lbl_score2.AutoSize = True
        Me.lbl_score2.Location = New System.Drawing.Point(93, 20)
        Me.lbl_score2.Name = "lbl_score2"
        Me.lbl_score2.Size = New System.Drawing.Size(65, 18)
        Me.lbl_score2.TabIndex = 28
        Me.lbl_score2.Text = "Score :"
        '
        'grp_box_nom_j3
        '
        Me.grp_box_nom_j3.Controls.Add(Me.lbl_score3)
        Me.grp_box_nom_j3.Controls.Add(Me.lbl_points3)
        Me.grp_box_nom_j3.Controls.Add(Me.lbl_pts_j3)
        Me.grp_box_nom_j3.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_nom_j3.Location = New System.Drawing.Point(12, 16)
        Me.grp_box_nom_j3.Name = "grp_box_nom_j3"
        Me.grp_box_nom_j3.Size = New System.Drawing.Size(393, 50)
        Me.grp_box_nom_j3.TabIndex = 32
        Me.grp_box_nom_j3.TabStop = False
        Me.grp_box_nom_j3.Text = "Nom 3"
        '
        'lbl_score3
        '
        Me.lbl_score3.AutoSize = True
        Me.lbl_score3.Location = New System.Drawing.Point(95, 20)
        Me.lbl_score3.Name = "lbl_score3"
        Me.lbl_score3.Size = New System.Drawing.Size(65, 18)
        Me.lbl_score3.TabIndex = 27
        Me.lbl_score3.Text = "Score :"
        '
        'grp_box_pieces_j2
        '
        Me.grp_box_pieces_j2.Controls.Add(Me.pic_piece_22)
        Me.grp_box_pieces_j2.Controls.Add(Me.pic_piece_23)
        Me.grp_box_pieces_j2.Controls.Add(Me.pic_piece_24)
        Me.grp_box_pieces_j2.Controls.Add(Me.pic_piece_25)
        Me.grp_box_pieces_j2.Controls.Add(Me.pic_piece_26)
        Me.grp_box_pieces_j2.Controls.Add(Me.pic_piece_21)
        Me.grp_box_pieces_j2.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_pieces_j2.Location = New System.Drawing.Point(14, 75)
        Me.grp_box_pieces_j2.Name = "grp_box_pieces_j2"
        Me.grp_box_pieces_j2.Size = New System.Drawing.Size(393, 81)
        Me.grp_box_pieces_j2.TabIndex = 34
        Me.grp_box_pieces_j2.TabStop = False
        Me.grp_box_pieces_j2.Text = "Pièces"
        '
        'pic_piece_22
        '
        Me.pic_piece_22.Location = New System.Drawing.Point(83, 18)
        Me.pic_piece_22.Name = "pic_piece_22"
        Me.pic_piece_22.Size = New System.Drawing.Size(53, 50)
        Me.pic_piece_22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_22.TabIndex = 15
        Me.pic_piece_22.TabStop = False
        '
        'pic_piece_23
        '
        Me.pic_piece_23.Location = New System.Drawing.Point(144, 18)
        Me.pic_piece_23.Name = "pic_piece_23"
        Me.pic_piece_23.Size = New System.Drawing.Size(48, 50)
        Me.pic_piece_23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_23.TabIndex = 14
        Me.pic_piece_23.TabStop = False
        '
        'pic_piece_24
        '
        Me.pic_piece_24.Location = New System.Drawing.Point(198, 18)
        Me.pic_piece_24.Name = "pic_piece_24"
        Me.pic_piece_24.Size = New System.Drawing.Size(47, 50)
        Me.pic_piece_24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_24.TabIndex = 13
        Me.pic_piece_24.TabStop = False
        '
        'pic_piece_25
        '
        Me.pic_piece_25.Location = New System.Drawing.Point(251, 18)
        Me.pic_piece_25.Name = "pic_piece_25"
        Me.pic_piece_25.Size = New System.Drawing.Size(53, 50)
        Me.pic_piece_25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_25.TabIndex = 12
        Me.pic_piece_25.TabStop = False
        '
        'pic_piece_26
        '
        Me.pic_piece_26.Location = New System.Drawing.Point(310, 18)
        Me.pic_piece_26.Name = "pic_piece_26"
        Me.pic_piece_26.Size = New System.Drawing.Size(52, 50)
        Me.pic_piece_26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_26.TabIndex = 11
        Me.pic_piece_26.TabStop = False
        '
        'pic_piece_21
        '
        Me.pic_piece_21.Location = New System.Drawing.Point(28, 18)
        Me.pic_piece_21.Name = "pic_piece_21"
        Me.pic_piece_21.Size = New System.Drawing.Size(49, 50)
        Me.pic_piece_21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_21.TabIndex = 10
        Me.pic_piece_21.TabStop = False
        '
        'grp_box_pieces_j3
        '
        Me.grp_box_pieces_j3.Controls.Add(Me.pic_piece_32)
        Me.grp_box_pieces_j3.Controls.Add(Me.pic_piece_33)
        Me.grp_box_pieces_j3.Controls.Add(Me.pic_piece_34)
        Me.grp_box_pieces_j3.Controls.Add(Me.pic_piece_35)
        Me.grp_box_pieces_j3.Controls.Add(Me.pic_piece_36)
        Me.grp_box_pieces_j3.Controls.Add(Me.pic_piece_31)
        Me.grp_box_pieces_j3.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_pieces_j3.Location = New System.Drawing.Point(12, 72)
        Me.grp_box_pieces_j3.Name = "grp_box_pieces_j3"
        Me.grp_box_pieces_j3.Size = New System.Drawing.Size(393, 81)
        Me.grp_box_pieces_j3.TabIndex = 29
        Me.grp_box_pieces_j3.TabStop = False
        Me.grp_box_pieces_j3.Text = "Pièces "
        '
        'pic_piece_32
        '
        Me.pic_piece_32.Location = New System.Drawing.Point(83, 18)
        Me.pic_piece_32.Name = "pic_piece_32"
        Me.pic_piece_32.Size = New System.Drawing.Size(53, 50)
        Me.pic_piece_32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_32.TabIndex = 15
        Me.pic_piece_32.TabStop = False
        '
        'pic_piece_33
        '
        Me.pic_piece_33.Location = New System.Drawing.Point(144, 18)
        Me.pic_piece_33.Name = "pic_piece_33"
        Me.pic_piece_33.Size = New System.Drawing.Size(48, 50)
        Me.pic_piece_33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_33.TabIndex = 14
        Me.pic_piece_33.TabStop = False
        '
        'pic_piece_34
        '
        Me.pic_piece_34.Location = New System.Drawing.Point(198, 18)
        Me.pic_piece_34.Name = "pic_piece_34"
        Me.pic_piece_34.Size = New System.Drawing.Size(47, 50)
        Me.pic_piece_34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_34.TabIndex = 13
        Me.pic_piece_34.TabStop = False
        '
        'pic_piece_35
        '
        Me.pic_piece_35.Location = New System.Drawing.Point(251, 18)
        Me.pic_piece_35.Name = "pic_piece_35"
        Me.pic_piece_35.Size = New System.Drawing.Size(53, 50)
        Me.pic_piece_35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_35.TabIndex = 12
        Me.pic_piece_35.TabStop = False
        '
        'pic_piece_36
        '
        Me.pic_piece_36.Location = New System.Drawing.Point(310, 18)
        Me.pic_piece_36.Name = "pic_piece_36"
        Me.pic_piece_36.Size = New System.Drawing.Size(52, 50)
        Me.pic_piece_36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_36.TabIndex = 11
        Me.pic_piece_36.TabStop = False
        '
        'pic_piece_31
        '
        Me.pic_piece_31.Location = New System.Drawing.Point(28, 18)
        Me.pic_piece_31.Name = "pic_piece_31"
        Me.pic_piece_31.Size = New System.Drawing.Size(49, 50)
        Me.pic_piece_31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_31.TabIndex = 10
        Me.pic_piece_31.TabStop = False
        '
        'grp_box_J1
        '
        Me.grp_box_J1.Controls.Add(Me.grp_box_nom_j1)
        Me.grp_box_J1.Controls.Add(Me.grp_box_pieces_j1)
        Me.grp_box_J1.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_J1.Location = New System.Drawing.Point(1067, 45)
        Me.grp_box_J1.Name = "grp_box_J1"
        Me.grp_box_J1.Size = New System.Drawing.Size(422, 176)
        Me.grp_box_J1.TabIndex = 36
        Me.grp_box_J1.TabStop = False
        Me.grp_box_J1.Text = "Joueur 1"
        '
        'grp_box_J2
        '
        Me.grp_box_J2.Controls.Add(Me.grp_box_pieces_j2)
        Me.grp_box_J2.Controls.Add(Me.grp_box_nom_j2)
        Me.grp_box_J2.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_J2.Location = New System.Drawing.Point(1070, 227)
        Me.grp_box_J2.Name = "grp_box_J2"
        Me.grp_box_J2.Size = New System.Drawing.Size(419, 165)
        Me.grp_box_J2.TabIndex = 37
        Me.grp_box_J2.TabStop = False
        Me.grp_box_J2.Text = "Joueur 2"
        '
        'grp_box_J3
        '
        Me.grp_box_J3.Controls.Add(Me.grp_box_pieces_j3)
        Me.grp_box_J3.Controls.Add(Me.grp_box_nom_j3)
        Me.grp_box_J3.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_J3.Location = New System.Drawing.Point(1070, 398)
        Me.grp_box_J3.Name = "grp_box_J3"
        Me.grp_box_J3.Size = New System.Drawing.Size(416, 163)
        Me.grp_box_J3.TabIndex = 38
        Me.grp_box_J3.TabStop = False
        Me.grp_box_J3.Text = "Joueur 3"
        '
        'grp_box_nom_j4
        '
        Me.grp_box_nom_j4.Controls.Add(Me.lbl_score4)
        Me.grp_box_nom_j4.Controls.Add(Me.lbl_points4)
        Me.grp_box_nom_j4.Controls.Add(Me.lbl_pts_j4)
        Me.grp_box_nom_j4.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_nom_j4.Location = New System.Drawing.Point(11, 21)
        Me.grp_box_nom_j4.Name = "grp_box_nom_j4"
        Me.grp_box_nom_j4.Size = New System.Drawing.Size(393, 48)
        Me.grp_box_nom_j4.TabIndex = 33
        Me.grp_box_nom_j4.TabStop = False
        Me.grp_box_nom_j4.Text = "Nom 4"
        '
        'lbl_score4
        '
        Me.lbl_score4.AutoSize = True
        Me.lbl_score4.Location = New System.Drawing.Point(94, 20)
        Me.lbl_score4.Name = "lbl_score4"
        Me.lbl_score4.Size = New System.Drawing.Size(65, 18)
        Me.lbl_score4.TabIndex = 28
        Me.lbl_score4.Text = "Score :"
        '
        'lbl_points4
        '
        Me.lbl_points4.AutoSize = True
        Me.lbl_points4.Location = New System.Drawing.Point(238, 20)
        Me.lbl_points4.Name = "lbl_points4"
        Me.lbl_points4.Size = New System.Drawing.Size(58, 18)
        Me.lbl_points4.TabIndex = 25
        Me.lbl_points4.Text = "points"
        '
        'lbl_pts_j4
        '
        Me.lbl_pts_j4.AutoSize = True
        Me.lbl_pts_j4.Location = New System.Drawing.Point(165, 20)
        Me.lbl_pts_j4.Name = "lbl_pts_j4"
        Me.lbl_pts_j4.Size = New System.Drawing.Size(60, 18)
        Me.lbl_pts_j4.TabIndex = 7
        Me.lbl_pts_j4.Text = "pts_j4"
        '
        'grp_box_pieces_j4
        '
        Me.grp_box_pieces_j4.Controls.Add(Me.pic_piece_42)
        Me.grp_box_pieces_j4.Controls.Add(Me.pic_piece_43)
        Me.grp_box_pieces_j4.Controls.Add(Me.pic_piece_44)
        Me.grp_box_pieces_j4.Controls.Add(Me.pic_piece_45)
        Me.grp_box_pieces_j4.Controls.Add(Me.pic_piece_46)
        Me.grp_box_pieces_j4.Controls.Add(Me.pic_piece_41)
        Me.grp_box_pieces_j4.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_pieces_j4.Location = New System.Drawing.Point(11, 75)
        Me.grp_box_pieces_j4.Name = "grp_box_pieces_j4"
        Me.grp_box_pieces_j4.Size = New System.Drawing.Size(393, 81)
        Me.grp_box_pieces_j4.TabIndex = 35
        Me.grp_box_pieces_j4.TabStop = False
        Me.grp_box_pieces_j4.Text = "Pièces "
        '
        'pic_piece_42
        '
        Me.pic_piece_42.Location = New System.Drawing.Point(83, 18)
        Me.pic_piece_42.Name = "pic_piece_42"
        Me.pic_piece_42.Size = New System.Drawing.Size(53, 50)
        Me.pic_piece_42.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_42.TabIndex = 15
        Me.pic_piece_42.TabStop = False
        '
        'pic_piece_43
        '
        Me.pic_piece_43.Location = New System.Drawing.Point(144, 18)
        Me.pic_piece_43.Name = "pic_piece_43"
        Me.pic_piece_43.Size = New System.Drawing.Size(48, 50)
        Me.pic_piece_43.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_43.TabIndex = 14
        Me.pic_piece_43.TabStop = False
        '
        'pic_piece_44
        '
        Me.pic_piece_44.Location = New System.Drawing.Point(198, 18)
        Me.pic_piece_44.Name = "pic_piece_44"
        Me.pic_piece_44.Size = New System.Drawing.Size(47, 50)
        Me.pic_piece_44.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_44.TabIndex = 13
        Me.pic_piece_44.TabStop = False
        '
        'pic_piece_45
        '
        Me.pic_piece_45.Location = New System.Drawing.Point(251, 18)
        Me.pic_piece_45.Name = "pic_piece_45"
        Me.pic_piece_45.Size = New System.Drawing.Size(53, 50)
        Me.pic_piece_45.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_45.TabIndex = 12
        Me.pic_piece_45.TabStop = False
        '
        'pic_piece_46
        '
        Me.pic_piece_46.Location = New System.Drawing.Point(310, 18)
        Me.pic_piece_46.Name = "pic_piece_46"
        Me.pic_piece_46.Size = New System.Drawing.Size(52, 50)
        Me.pic_piece_46.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_46.TabIndex = 11
        Me.pic_piece_46.TabStop = False
        '
        'pic_piece_41
        '
        Me.pic_piece_41.Location = New System.Drawing.Point(28, 18)
        Me.pic_piece_41.Name = "pic_piece_41"
        Me.pic_piece_41.Size = New System.Drawing.Size(49, 50)
        Me.pic_piece_41.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_piece_41.TabIndex = 10
        Me.pic_piece_41.TabStop = False
        '
        'grp_box_J4
        '
        Me.grp_box_J4.Controls.Add(Me.grp_box_pieces_j4)
        Me.grp_box_J4.Controls.Add(Me.grp_box_nom_j4)
        Me.grp_box_J4.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_box_J4.Location = New System.Drawing.Point(1072, 567)
        Me.grp_box_J4.Name = "grp_box_J4"
        Me.grp_box_J4.Size = New System.Drawing.Size(414, 164)
        Me.grp_box_J4.TabIndex = 39
        Me.grp_box_J4.TabStop = False
        Me.grp_box_J4.Text = "Joueur 4"
        '
        'lbl_test
        '
        Me.lbl_test.AutoSize = True
        Me.lbl_test.Location = New System.Drawing.Point(1081, 9)
        Me.lbl_test.Name = "lbl_test"
        Me.lbl_test.Size = New System.Drawing.Size(51, 17)
        Me.lbl_test.TabIndex = 40
        Me.lbl_test.Text = "Label1"
        '
        'frm_tableau
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Menu
        Me.ClientSize = New System.Drawing.Size(1741, 971)
        Me.Controls.Add(Me.lbl_test)
        Me.Controls.Add(Me.grp_box_J4)
        Me.Controls.Add(Me.grp_box_J3)
        Me.Controls.Add(Me.grp_box_J2)
        Me.Controls.Add(Me.grp_box_J1)
        Me.Controls.Add(Me.cmd_aide)
        Me.Controls.Add(Me.cmd_valider)
        Me.Controls.Add(Me.cmd_passer)
        Me.Controls.Add(Me.cmd_quitter)
        Me.Controls.Add(Me.cmd_echanger)
        Me.Name = "frm_tableau"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Qwirkle"
        Me.grp_box_pieces_j1.ResumeLayout(False)
        CType(Me.pic_piece_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_box_nom_j1.ResumeLayout(False)
        Me.grp_box_nom_j1.PerformLayout()
        Me.grp_box_nom_j2.ResumeLayout(False)
        Me.grp_box_nom_j2.PerformLayout()
        Me.grp_box_nom_j3.ResumeLayout(False)
        Me.grp_box_nom_j3.PerformLayout()
        Me.grp_box_pieces_j2.ResumeLayout(False)
        CType(Me.pic_piece_22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_box_pieces_j3.ResumeLayout(False)
        CType(Me.pic_piece_32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_31, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_box_J1.ResumeLayout(False)
        Me.grp_box_J2.ResumeLayout(False)
        Me.grp_box_J3.ResumeLayout(False)
        Me.grp_box_nom_j4.ResumeLayout(False)
        Me.grp_box_nom_j4.PerformLayout()
        Me.grp_box_pieces_j4.ResumeLayout(False)
        CType(Me.pic_piece_42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_piece_41, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_box_J4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_pts_j1 As Label
    Friend WithEvents lbl_pts_j3 As Label
    Friend WithEvents lbl_pts_j2 As Label
    Friend WithEvents pic_piece_16 As PictureBox
    Friend WithEvents pic_piece_15 As PictureBox
    Friend WithEvents pic_piece_14 As PictureBox
    Friend WithEvents cmd_echanger As Button
    Friend WithEvents cmd_quitter As Button
    Friend WithEvents cmd_passer As Button
    Friend WithEvents cmd_valider As Button
    Friend WithEvents cmd_aide As Button
    Friend WithEvents lbl_points1 As Label
    Friend WithEvents lbl_points3 As Label
    Friend WithEvents lbl_points2 As Label
    Friend WithEvents pic_piece_13 As PictureBox
    Friend WithEvents pic_piece_12 As PictureBox
    Friend WithEvents grp_box_pieces_j1 As GroupBox
    Friend WithEvents grp_box_nom_j1 As GroupBox
    Friend WithEvents grp_box_nom_j2 As GroupBox
    Friend WithEvents grp_box_nom_j3 As GroupBox
    Friend WithEvents grp_box_pieces_j2 As GroupBox
    Friend WithEvents pic_piece_22 As PictureBox
    Friend WithEvents pic_piece_23 As PictureBox
    Friend WithEvents pic_piece_24 As PictureBox
    Friend WithEvents pic_piece_25 As PictureBox
    Friend WithEvents pic_piece_26 As PictureBox
    Friend WithEvents grp_box_pieces_j3 As GroupBox
    Friend WithEvents pic_piece_32 As PictureBox
    Friend WithEvents pic_piece_33 As PictureBox
    Friend WithEvents pic_piece_34 As PictureBox
    Friend WithEvents pic_piece_35 As PictureBox
    Friend WithEvents pic_piece_36 As PictureBox
    Friend WithEvents pic_piece_11 As PictureBox
    Friend WithEvents pic_piece_21 As PictureBox
    Friend WithEvents pic_piece_31 As PictureBox
    Friend WithEvents tlt_aide_plateau As ToolTip
    Friend WithEvents lbl_score1 As Label
    Friend WithEvents lbl_score2 As Label
    Friend WithEvents lbl_score3 As Label
    Friend WithEvents grp_box_J1 As GroupBox
    Friend WithEvents grp_box_J2 As GroupBox
    Friend WithEvents grp_box_J3 As GroupBox
    Friend WithEvents grp_box_nom_j4 As GroupBox
    Friend WithEvents lbl_score4 As Label
    Friend WithEvents lbl_points4 As Label
    Friend WithEvents lbl_pts_j4 As Label
    Friend WithEvents grp_box_pieces_j4 As GroupBox
    Friend WithEvents pic_piece_42 As PictureBox
    Friend WithEvents pic_piece_43 As PictureBox
    Friend WithEvents pic_piece_44 As PictureBox
    Friend WithEvents pic_piece_45 As PictureBox
    Friend WithEvents pic_piece_46 As PictureBox
    Friend WithEvents grp_box_J4 As GroupBox
    Friend WithEvents lbl_test As Label
    Friend WithEvents pic_piece_41 As PictureBox
End Class
