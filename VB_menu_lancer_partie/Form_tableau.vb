﻿Imports QwirkleLib
Public Class frm_tableau

    Dim grille(30, 30) As PictureBox
    Private Sub Form_tableau_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pic_piece_12.AllowDrop = True
        pic_piece_11.AllowDrop = True

        Dim columns As Integer = 0
        Dim rows As Integer = 0
        Dim position_x As Integer = 100
        Dim position_y As Integer = 50
        Dim taille As System.Drawing.Size
        taille.Height = 20
        taille.Width = 20

        Dim lieu As Point
        While rows < 30
            While columns < 30
                grille(columns, rows) = New PictureBox
                grille(columns, rows).BorderStyle = BorderStyle.FixedSingle
                grille(columns, rows).SizeMode = PictureBoxSizeMode.StretchImage
                grille(columns, rows).Visible = True
                grille(columns, rows).AllowDrop = True
                grille(columns, rows).Size = taille
                lieu.X = position_x
                lieu.Y = position_y
                grille(columns, rows).Location = lieu
                Me.Controls.Add(grille(columns, rows))

                AddHandler grille(columns, rows).DragEnter, AddressOf PictureBox1_DragEnter
                AddHandler grille(columns, rows).DragDrop, AddressOf PictureBox1_DragDrop
                AddHandler grille(columns, rows).MouseMove, AddressOf PictureBox1_MouseMove

                position_x += 20
                If position_x = 700 Then
                    position_x = 100
                    position_y += 20
                End If
                columns += 1
            End While
            rows += 1
            columns = 0
        End While


        grp_box_nom_j1.Text = My.Forms.Frm_lancer_partie.txt_j1.Text
        grp_box_nom_j2.Text = My.Forms.Frm_lancer_partie.txt_j2.Text
        grp_box_nom_j3.Text = My.Forms.Frm_lancer_partie.txt_j3.Text
        grp_box_nom_j4.Text = My.Forms.Frm_lancer_partie.txt_j4.Text


        If My.Forms.Frm_lancer_partie.rdb_2_joueurs.Checked = True Then
            grp_box_J3.Visible = False
            grp_box_J4.Visible = False
        Else
            If My.Forms.Frm_lancer_partie.rdb_3_joueurs.Checked = True Then
                grp_box_J3.Visible = True
                grp_box_J4.Visible = False

            End If
        End If

        grp_box_J2.Enabled = False
        grp_box_J3.Enabled = False
        grp_box_J4.Enabled = False

        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized

        Dim Pioche1 As New Pioche()
        Dim Joueur1 As New Joueur(My.Forms.Frm_lancer_partie.txt_j1.Text)
        Dim Joueur2 As New Joueur(My.Forms.Frm_lancer_partie.txt_j2.Text)
        Dim Joueur3 As New Joueur(My.Forms.Frm_lancer_partie.txt_j3.Text)
        Dim Joueur4 As New Joueur(My.Forms.Frm_lancer_partie.txt_j4.Text)
        Dim JOUEURS(3) As Joueur

        Joueur1.setName(My.Forms.Frm_lancer_partie.txt_j1.Text)
        Joueur1.setMain_joueur(Pioche1.FirstDistributionTuiles())
        Joueur2.setName(My.Forms.Frm_lancer_partie.txt_j2.Text)
        Joueur2.setMain_joueur(Pioche1.FirstDistributionTuiles())
        Joueur3.setName(My.Forms.Frm_lancer_partie.txt_j3.Text)
        Joueur3.setMain_joueur(Pioche1.FirstDistributionTuiles())
        Joueur4.setName(My.Forms.Frm_lancer_partie.txt_j4.Text)
        Joueur4.setMain_joueur(Pioche1.FirstDistributionTuiles())

        Dim nombre_de_joueur As Integer

        If My.Forms.Frm_lancer_partie.rdb_2_joueurs.Checked = True Then
            JOUEURS(0) = Joueur1
            JOUEURS(1) = Joueur2
            JOUEURS(2) = Nothing
            JOUEURS(3) = Nothing
            nombre_de_joueur = 2
        End If

        If My.Forms.Frm_lancer_partie.rdb_3_joueurs.Checked = True Then
            JOUEURS(0) = Joueur1
            JOUEURS(1) = Joueur2
            JOUEURS(2) = Joueur3
            JOUEURS(3) = Nothing
            nombre_de_joueur = 3

        End If

        If My.Forms.Frm_lancer_partie.rdb_4_joueurs.Checked = True Then
            JOUEURS(0) = Joueur1
            JOUEURS(1) = Joueur2
            JOUEURS(2) = Joueur3
            JOUEURS(3) = Joueur4
            nombre_de_joueur = 4

        End If

        Dim i As Byte
        Dim j As Byte
        Dim mongrp As GroupBox
        For i = 1 To nombre_de_joueur


            mongrp = Me.Controls("grp_box_J" & i.ToString).Controls("grp_box_pieces_j" & i.ToString)
            Dim tuilesJoueur() As Tuile = JOUEURS(i - 1).getMain_joueur

            For j = 1 To 6
                Dim pic As PictureBox
                Dim nomtuile As String

                pic = mongrp.Controls("pic_piece_" & i.ToString & j.ToString)
                nomtuile = tuilesJoueur(j - 1).getForme & tuilesJoueur(j - 1).getCouleur

                pic.Image = My.Resources.ResourceManager.GetObject(nomtuile)
                pic.Visible = True

            Next
        Next
    End Sub



    Private Sub PictureBox1_MouseMove(sender As Object, e As MouseEventArgs) Handles pic_piece_11.MouseMove, pic_piece_16.MouseMove, pic_piece_15.MouseMove, pic_piece_14.MouseMove, pic_piece_13.MouseMove, pic_piece_12.MouseMove, pic_piece_46.MouseMove, pic_piece_36.MouseMove, pic_piece_26.MouseMove, pic_piece_45.MouseMove, pic_piece_35.MouseMove, pic_piece_25.MouseMove, pic_piece_44.MouseMove, pic_piece_34.MouseMove, pic_piece_24.MouseMove, pic_piece_43.MouseMove, pic_piece_33.MouseMove, pic_piece_23.MouseMove, pic_piece_42.MouseMove, pic_piece_32.MouseMove, pic_piece_22.MouseMove, pic_piece_41.MouseMove, pic_piece_31.MouseMove, pic_piece_21.MouseMove
        Dim effetRealise As DragDropEffects
        Dim pic As PictureBox = sender
        If e.Button = MouseButtons.Left AndAlso pic.Image IsNot Nothing Then
            pic.AllowDrop = False
            effetRealise = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If effetRealise = DragDropEffects.Move Then
                pic.Image = Nothing
            End If
            pic.AllowDrop = True

        End If
    End Sub

    Private Sub PictureBox1_DragDrop(sender As Object, e As DragEventArgs) Handles pic_piece_11.DragDrop, pic_piece_16.DragDrop, pic_piece_15.DragDrop, pic_piece_14.DragDrop, pic_piece_13.DragDrop, pic_piece_12.DragDrop, pic_piece_46.DragDrop, pic_piece_36.DragDrop, pic_piece_26.DragDrop, pic_piece_45.DragDrop, pic_piece_35.DragDrop, pic_piece_25.DragDrop, pic_piece_44.DragDrop, pic_piece_34.DragDrop, pic_piece_24.DragDrop, pic_piece_43.DragDrop, pic_piece_33.DragDrop, pic_piece_23.DragDrop, pic_piece_42.DragDrop, pic_piece_32.DragDrop, pic_piece_22.DragDrop, pic_piece_41.DragDrop, pic_piece_31.DragDrop, pic_piece_21.DragDrop
        '   If (grille Is ) Then
        Dim pic As PictureBox = sender
            pic.Image = e.Data.GetData(DataFormats.Bitmap)
        '  End If
    End Sub

    Private Sub PictureBox1_DragEnter(sender As Object, e As DragEventArgs) Handles pic_piece_11.DragEnter, pic_piece_16.DragEnter, pic_piece_15.DragEnter, pic_piece_14.DragEnter, pic_piece_13.DragEnter, pic_piece_12.DragEnter, pic_piece_46.DragEnter, pic_piece_36.DragEnter, pic_piece_26.DragEnter, pic_piece_45.DragEnter, pic_piece_35.DragEnter, pic_piece_25.DragEnter, pic_piece_44.DragEnter, pic_piece_34.DragEnter, pic_piece_24.DragEnter, pic_piece_43.DragEnter, pic_piece_33.DragEnter, pic_piece_23.DragEnter, pic_piece_42.DragEnter, pic_piece_32.DragEnter, pic_piece_22.DragEnter, pic_piece_41.DragEnter, pic_piece_31.DragEnter, pic_piece_21.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub cmd_quitter_Click(sender As Object, e As EventArgs) Handles cmd_quitter.Click
        MessageBox.Show("Voulez vous vraiment quitter ?", "Quitter ""Qwirkle.""", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        If MsgBoxResult.Ok Then
            Me.Close()
        End If
    End Sub
    Private Sub cmd_valider_Click(sender As Object, e As EventArgs) Handles cmd_valider.Click

        Dim distribution As New Pioche
        distribution.DistributionTuiles(main_joueur)

        If My.Forms.Frm_lancer_partie.rdb_2_joueurs.Checked = True Then



            If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False Then
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = True

            Else
                If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True Then
                    grp_box_J1.Enabled = True
                    grp_box_J2.Enabled = False
                End If

            End If

        ElseIf My.Forms.Frm_lancer_partie.rdb_3_joueurs.Checked = True Then

            If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False Then
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = True
                grp_box_J3.Enabled = False

            Else
                If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True And grp_box_J3.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = False
                    grp_box_J3.Enabled = True

                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = True Then
                        grp_box_J1.Enabled = True
                        grp_box_J2.Enabled = False
                        grp_box_J3.Enabled = False
                    End If
                End If

            End If

        ElseIf My.Forms.Frm_lancer_partie.rdb_4_joueurs.Checked = True Then


            If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False And grp_box_J4.Enabled = False Then
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = True
                grp_box_J3.Enabled = False
                grp_box_J4.Enabled = False

            Else
                If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True And grp_box_J3.Enabled = False And grp_box_J4.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = False
                    grp_box_J3.Enabled = True
                    grp_box_J4.Enabled = False

                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = True And grp_box_J4.Enabled = False Then
                        grp_box_J1.Enabled = False
                        grp_box_J2.Enabled = False
                        grp_box_J3.Enabled = False
                        grp_box_J4.Enabled = True

                    Else
                        If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False And grp_box_J4.Enabled = True Then
                            grp_box_J1.Enabled = True
                            grp_box_J2.Enabled = False
                            grp_box_J3.Enabled = False
                            grp_box_J4.Enabled = False
                        End If
                    End If
                End If
            End If
        End If

    End Sub
    Private Sub cmd_passer_Click(sender As Object, e As EventArgs) Handles cmd_passer.Click
        MessageBox.Show("Voulez vous vraiment passer ?", "Passer ""Qwirkle.""",
      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        If MsgBoxResult.Ok Then

            If My.Forms.Frm_lancer_partie.rdb_2_joueurs.Checked = True Then
                If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = True

                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True Then
                        grp_box_J1.Enabled = True
                        grp_box_J2.Enabled = False
                    End If

                End If

            ElseIf My.Forms.Frm_lancer_partie.rdb_3_joueurs.Checked = True Then

                If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = True
                    grp_box_J3.Enabled = False

                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True And grp_box_J3.Enabled = False Then
                        grp_box_J1.Enabled = False
                        grp_box_J2.Enabled = False
                        grp_box_J3.Enabled = True

                    Else
                        If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = True Then
                            grp_box_J1.Enabled = True
                            grp_box_J2.Enabled = False
                            grp_box_J3.Enabled = False
                        End If
                    End If

                End If

            ElseIf My.Forms.Frm_lancer_partie.rdb_4_joueurs.Checked = True Then


                If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False And grp_box_J4.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = True
                    grp_box_J3.Enabled = False
                    grp_box_J4.Enabled = False

                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True And grp_box_J3.Enabled = False And grp_box_J4.Enabled = False Then
                        grp_box_J1.Enabled = False
                        grp_box_J2.Enabled = False
                        grp_box_J3.Enabled = True
                        grp_box_J4.Enabled = False

                    Else
                        If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = True And grp_box_J4.Enabled = False Then
                            grp_box_J1.Enabled = False
                            grp_box_J2.Enabled = False
                            grp_box_J3.Enabled = False
                            grp_box_J4.Enabled = True

                        Else
                            If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False And grp_box_J4.Enabled = True Then
                                grp_box_J1.Enabled = True
                                grp_box_J2.Enabled = False
                                grp_box_J3.Enabled = False
                                grp_box_J4.Enabled = False
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cmd_aide_Click(sender As Object, e As EventArgs) Handles cmd_aide.Click
        System.Diagnostics.Process.Start("https://www.iello.fr/regles/Qwirkle_regles_FR_web.pdf")
    End Sub

End Class


