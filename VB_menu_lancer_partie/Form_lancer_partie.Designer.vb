﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_lancer_partie
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lbl_titre = New System.Windows.Forms.Label()
        Me.lbl_nb_joueur = New System.Windows.Forms.Label()
        Me.lbl_nom_j1 = New System.Windows.Forms.Label()
        Me.lbl_nom_j2 = New System.Windows.Forms.Label()
        Me.lbl_nom_j4 = New System.Windows.Forms.Label()
        Me.lbl_nom_j3 = New System.Windows.Forms.Label()
        Me.cmd_jouer = New System.Windows.Forms.Button()
        Me.cmd_aide = New System.Windows.Forms.Button()
        Me.txt_j1 = New System.Windows.Forms.TextBox()
        Me.txt_j2 = New System.Windows.Forms.TextBox()
        Me.txt_j4 = New System.Windows.Forms.TextBox()
        Me.txt_j3 = New System.Windows.Forms.TextBox()
        Me.rdb_2_joueurs = New System.Windows.Forms.RadioButton()
        Me.rdb_3_joueurs = New System.Windows.Forms.RadioButton()
        Me.rdb_4_joueurs = New System.Windows.Forms.RadioButton()
        Me.tlt_aide = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'lbl_titre
        '
        Me.lbl_titre.AutoSize = True
        Me.lbl_titre.BackColor = System.Drawing.Color.Transparent
        Me.lbl_titre.Font = New System.Drawing.Font("Snap ITC", 50.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_titre.ForeColor = System.Drawing.Color.Firebrick
        Me.lbl_titre.Location = New System.Drawing.Point(178, 9)
        Me.lbl_titre.Name = "lbl_titre"
        Me.lbl_titre.Size = New System.Drawing.Size(424, 108)
        Me.lbl_titre.TabIndex = 0
        Me.lbl_titre.Text = "Qwirkle"
        '
        'lbl_nb_joueur
        '
        Me.lbl_nb_joueur.AutoSize = True
        Me.lbl_nb_joueur.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nb_joueur.Font = New System.Drawing.Font("Snap ITC", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nb_joueur.Location = New System.Drawing.Point(26, 132)
        Me.lbl_nb_joueur.Name = "lbl_nb_joueur"
        Me.lbl_nb_joueur.Size = New System.Drawing.Size(302, 32)
        Me.lbl_nb_joueur.TabIndex = 1
        Me.lbl_nb_joueur.Text = "Nombre de joueurs :"
        '
        'lbl_nom_j1
        '
        Me.lbl_nom_j1.AutoSize = True
        Me.lbl_nom_j1.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nom_j1.Font = New System.Drawing.Font("Snap ITC", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom_j1.Location = New System.Drawing.Point(57, 211)
        Me.lbl_nom_j1.Name = "lbl_nom_j1"
        Me.lbl_nom_j1.Size = New System.Drawing.Size(146, 23)
        Me.lbl_nom_j1.TabIndex = 2
        Me.lbl_nom_j1.Text = "Nom joueur 1 :"
        '
        'lbl_nom_j2
        '
        Me.lbl_nom_j2.AutoSize = True
        Me.lbl_nom_j2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nom_j2.Font = New System.Drawing.Font("Snap ITC", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom_j2.Location = New System.Drawing.Point(428, 211)
        Me.lbl_nom_j2.Name = "lbl_nom_j2"
        Me.lbl_nom_j2.Size = New System.Drawing.Size(151, 23)
        Me.lbl_nom_j2.TabIndex = 3
        Me.lbl_nom_j2.Text = "Nom joueur 2 :"
        '
        'lbl_nom_j4
        '
        Me.lbl_nom_j4.AutoSize = True
        Me.lbl_nom_j4.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nom_j4.Font = New System.Drawing.Font("Snap ITC", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom_j4.Location = New System.Drawing.Point(428, 283)
        Me.lbl_nom_j4.Name = "lbl_nom_j4"
        Me.lbl_nom_j4.Size = New System.Drawing.Size(153, 23)
        Me.lbl_nom_j4.TabIndex = 4
        Me.lbl_nom_j4.Text = "Nom joueur 4 :"
        '
        'lbl_nom_j3
        '
        Me.lbl_nom_j3.AutoSize = True
        Me.lbl_nom_j3.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nom_j3.Font = New System.Drawing.Font("Snap ITC", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom_j3.Location = New System.Drawing.Point(57, 281)
        Me.lbl_nom_j3.Name = "lbl_nom_j3"
        Me.lbl_nom_j3.Size = New System.Drawing.Size(152, 23)
        Me.lbl_nom_j3.TabIndex = 5
        Me.lbl_nom_j3.Text = "Nom joueur 3 :"
        '
        'cmd_jouer
        '
        Me.cmd_jouer.BackColor = System.Drawing.Color.Transparent
        Me.cmd_jouer.Font = New System.Drawing.Font("Snap ITC", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_jouer.Location = New System.Drawing.Point(227, 365)
        Me.cmd_jouer.Name = "cmd_jouer"
        Me.cmd_jouer.Size = New System.Drawing.Size(320, 63)
        Me.cmd_jouer.TabIndex = 7
        Me.cmd_jouer.Text = "Jouer "
        Me.cmd_jouer.UseVisualStyleBackColor = False
        '
        'cmd_aide
        '
        Me.cmd_aide.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_aide.Location = New System.Drawing.Point(12, 390)
        Me.cmd_aide.Name = "cmd_aide"
        Me.cmd_aide.Size = New System.Drawing.Size(60, 38)
        Me.cmd_aide.TabIndex = 8
        Me.cmd_aide.Text = "Aide"
        Me.tlt_aide.SetToolTip(Me.cmd_aide, "Choissisez le nombre de joueurs en cliquant sur le boutton a  côté, écrivez vos n" &
        "oms." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Vous pouvez après cela, lancer la partie en cliquant sur ""Jouer""." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.cmd_aide.UseVisualStyleBackColor = True
        '
        'txt_j1
        '
        Me.txt_j1.Location = New System.Drawing.Point(209, 213)
        Me.txt_j1.Name = "txt_j1"
        Me.txt_j1.Size = New System.Drawing.Size(146, 22)
        Me.txt_j1.TabIndex = 9
        '
        'txt_j2
        '
        Me.txt_j2.Location = New System.Drawing.Point(584, 213)
        Me.txt_j2.Name = "txt_j2"
        Me.txt_j2.Size = New System.Drawing.Size(146, 22)
        Me.txt_j2.TabIndex = 10
        '
        'txt_j4
        '
        Me.txt_j4.Location = New System.Drawing.Point(584, 283)
        Me.txt_j4.Name = "txt_j4"
        Me.txt_j4.Size = New System.Drawing.Size(146, 22)
        Me.txt_j4.TabIndex = 11
        '
        'txt_j3
        '
        Me.txt_j3.Location = New System.Drawing.Point(209, 281)
        Me.txt_j3.Name = "txt_j3"
        Me.txt_j3.Size = New System.Drawing.Size(146, 22)
        Me.txt_j3.TabIndex = 12
        '
        'rdb_2_joueurs
        '
        Me.rdb_2_joueurs.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdb_2_joueurs.AutoSize = True
        Me.rdb_2_joueurs.BackColor = System.Drawing.Color.Transparent
        Me.rdb_2_joueurs.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdb_2_joueurs.Location = New System.Drawing.Point(334, 137)
        Me.rdb_2_joueurs.Name = "rdb_2_joueurs"
        Me.rdb_2_joueurs.Size = New System.Drawing.Size(93, 28)
        Me.rdb_2_joueurs.TabIndex = 13
        Me.rdb_2_joueurs.TabStop = True
        Me.rdb_2_joueurs.Text = "2 joueurs"
        Me.rdb_2_joueurs.UseVisualStyleBackColor = False
        '
        'rdb_3_joueurs
        '
        Me.rdb_3_joueurs.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdb_3_joueurs.AutoSize = True
        Me.rdb_3_joueurs.BackColor = System.Drawing.Color.Transparent
        Me.rdb_3_joueurs.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdb_3_joueurs.Location = New System.Drawing.Point(451, 137)
        Me.rdb_3_joueurs.Name = "rdb_3_joueurs"
        Me.rdb_3_joueurs.Size = New System.Drawing.Size(93, 28)
        Me.rdb_3_joueurs.TabIndex = 14
        Me.rdb_3_joueurs.TabStop = True
        Me.rdb_3_joueurs.Text = "3 joueurs"
        Me.rdb_3_joueurs.UseVisualStyleBackColor = False
        '
        'rdb_4_joueurs
        '
        Me.rdb_4_joueurs.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdb_4_joueurs.AutoSize = True
        Me.rdb_4_joueurs.BackColor = System.Drawing.Color.Transparent
        Me.rdb_4_joueurs.Font = New System.Drawing.Font("Snap ITC", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdb_4_joueurs.Location = New System.Drawing.Point(584, 137)
        Me.rdb_4_joueurs.Name = "rdb_4_joueurs"
        Me.rdb_4_joueurs.Size = New System.Drawing.Size(94, 28)
        Me.rdb_4_joueurs.TabIndex = 15
        Me.rdb_4_joueurs.TabStop = True
        Me.rdb_4_joueurs.Text = "4 joueurs"
        Me.rdb_4_joueurs.UseVisualStyleBackColor = False
        '
        'tlt_aide
        '
        Me.tlt_aide.AutoPopDelay = 10000
        Me.tlt_aide.InitialDelay = 300
        Me.tlt_aide.ReshowDelay = 100
        Me.tlt_aide.Tag = "Choisissez le nombre de joueurs et écrivez vos noms"
        Me.tlt_aide.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.tlt_aide.ToolTipTitle = "Aide"
        '
        'Frm_lancer_partie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.BackgroundImage = Global.VB_menu_lancer_partie.My.Resources.Resources.ww
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.rdb_4_joueurs)
        Me.Controls.Add(Me.rdb_3_joueurs)
        Me.Controls.Add(Me.rdb_2_joueurs)
        Me.Controls.Add(Me.txt_j3)
        Me.Controls.Add(Me.txt_j4)
        Me.Controls.Add(Me.txt_j2)
        Me.Controls.Add(Me.txt_j1)
        Me.Controls.Add(Me.cmd_aide)
        Me.Controls.Add(Me.cmd_jouer)
        Me.Controls.Add(Me.lbl_nom_j3)
        Me.Controls.Add(Me.lbl_nom_j4)
        Me.Controls.Add(Me.lbl_nom_j2)
        Me.Controls.Add(Me.lbl_nom_j1)
        Me.Controls.Add(Me.lbl_nb_joueur)
        Me.Controls.Add(Me.lbl_titre)
        Me.Name = "Frm_lancer_partie"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Qwirkle"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_titre As Label
    Friend WithEvents lbl_nb_joueur As Label
    Friend WithEvents lbl_nom_j1 As Label
    Friend WithEvents lbl_nom_j2 As Label
    Friend WithEvents lbl_nom_j4 As Label
    Friend WithEvents lbl_nom_j3 As Label
    Friend WithEvents cmd_jouer As Button
    Friend WithEvents cmd_aide As Button
    Friend WithEvents txt_j1 As TextBox
    Friend WithEvents txt_j2 As TextBox
    Friend WithEvents txt_j4 As TextBox
    Friend WithEvents txt_j3 As TextBox
    Friend WithEvents rdb_2_joueurs As RadioButton
    Friend WithEvents rdb_3_joueurs As RadioButton
    Friend WithEvents rdb_4_joueurs As RadioButton
    Friend WithEvents tlt_aide As ToolTip
End Class
