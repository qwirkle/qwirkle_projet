﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_fin_de_partie
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_fin_de_partie = New System.Windows.Forms.Label()
        Me.lbl_fin_points_j4 = New System.Windows.Forms.Label()
        Me.lbl_fin_j4 = New System.Windows.Forms.Label()
        Me.lbl_fin_points_j2 = New System.Windows.Forms.Label()
        Me.lbl_fin_j2 = New System.Windows.Forms.Label()
        Me.lbl_fin_points_j3 = New System.Windows.Forms.Label()
        Me.lbl_fin_j3 = New System.Windows.Forms.Label()
        Me.lbl_fin_points_j1 = New System.Windows.Forms.Label()
        Me.lbl_fin_j1 = New System.Windows.Forms.Label()
        Me.pnl_j1 = New System.Windows.Forms.Panel()
        Me.pnl_j2 = New System.Windows.Forms.Panel()
        Me.pnl_j3 = New System.Windows.Forms.Panel()
        Me.pnl_j4 = New System.Windows.Forms.Panel()
        Me.pnl_j1.SuspendLayout()
        Me.pnl_j2.SuspendLayout()
        Me.pnl_j3.SuspendLayout()
        Me.pnl_j4.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_fin_de_partie
        '
        Me.lbl_fin_de_partie.AutoSize = True
        Me.lbl_fin_de_partie.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin_de_partie.Font = New System.Drawing.Font("Snap ITC", 44.0!)
        Me.lbl_fin_de_partie.ForeColor = System.Drawing.Color.Firebrick
        Me.lbl_fin_de_partie.Location = New System.Drawing.Point(41, 18)
        Me.lbl_fin_de_partie.Name = "lbl_fin_de_partie"
        Me.lbl_fin_de_partie.Size = New System.Drawing.Size(703, 95)
        Me.lbl_fin_de_partie.TabIndex = 0
        Me.lbl_fin_de_partie.Text = "Fin de la partie"
        '
        'lbl_fin_points_j4
        '
        Me.lbl_fin_points_j4.AutoSize = True
        Me.lbl_fin_points_j4.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin_points_j4.Font = New System.Drawing.Font("Snap ITC", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fin_points_j4.Location = New System.Drawing.Point(109, 50)
        Me.lbl_fin_points_j4.Name = "lbl_fin_points_j4"
        Me.lbl_fin_points_j4.Size = New System.Drawing.Size(39, 36)
        Me.lbl_fin_points_j4.TabIndex = 1
        Me.lbl_fin_points_j4.Text = "0"
        '
        'lbl_fin_j4
        '
        Me.lbl_fin_j4.AutoSize = True
        Me.lbl_fin_j4.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin_j4.Font = New System.Drawing.Font("Snap ITC", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fin_j4.Location = New System.Drawing.Point(60, 1)
        Me.lbl_fin_j4.Name = "lbl_fin_j4"
        Me.lbl_fin_j4.Size = New System.Drawing.Size(162, 36)
        Me.lbl_fin_j4.TabIndex = 2
        Me.lbl_fin_j4.Text = "Joueur 4"
        '
        'lbl_fin_points_j2
        '
        Me.lbl_fin_points_j2.AutoSize = True
        Me.lbl_fin_points_j2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin_points_j2.Font = New System.Drawing.Font("Snap ITC", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fin_points_j2.Location = New System.Drawing.Point(109, 87)
        Me.lbl_fin_points_j2.Name = "lbl_fin_points_j2"
        Me.lbl_fin_points_j2.Size = New System.Drawing.Size(39, 36)
        Me.lbl_fin_points_j2.TabIndex = 3
        Me.lbl_fin_points_j2.Text = "0"
        '
        'lbl_fin_j2
        '
        Me.lbl_fin_j2.AutoSize = True
        Me.lbl_fin_j2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin_j2.Font = New System.Drawing.Font("Snap ITC", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fin_j2.Location = New System.Drawing.Point(59, 43)
        Me.lbl_fin_j2.Name = "lbl_fin_j2"
        Me.lbl_fin_j2.Size = New System.Drawing.Size(159, 36)
        Me.lbl_fin_j2.TabIndex = 4
        Me.lbl_fin_j2.Text = "Joueur 2"
        '
        'lbl_fin_points_j3
        '
        Me.lbl_fin_points_j3.AutoSize = True
        Me.lbl_fin_points_j3.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin_points_j3.Font = New System.Drawing.Font("Snap ITC", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fin_points_j3.Location = New System.Drawing.Point(108, 49)
        Me.lbl_fin_points_j3.Name = "lbl_fin_points_j3"
        Me.lbl_fin_points_j3.Size = New System.Drawing.Size(39, 36)
        Me.lbl_fin_points_j3.TabIndex = 5
        Me.lbl_fin_points_j3.Text = "0"
        '
        'lbl_fin_j3
        '
        Me.lbl_fin_j3.AutoSize = True
        Me.lbl_fin_j3.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin_j3.Font = New System.Drawing.Font("Snap ITC", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fin_j3.Location = New System.Drawing.Point(59, 0)
        Me.lbl_fin_j3.Name = "lbl_fin_j3"
        Me.lbl_fin_j3.Size = New System.Drawing.Size(161, 36)
        Me.lbl_fin_j3.TabIndex = 6
        Me.lbl_fin_j3.Text = "Joueur 3"
        '
        'lbl_fin_points_j1
        '
        Me.lbl_fin_points_j1.AutoSize = True
        Me.lbl_fin_points_j1.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin_points_j1.Font = New System.Drawing.Font("Snap ITC", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fin_points_j1.Location = New System.Drawing.Point(90, 77)
        Me.lbl_fin_points_j1.Name = "lbl_fin_points_j1"
        Me.lbl_fin_points_j1.Size = New System.Drawing.Size(39, 36)
        Me.lbl_fin_points_j1.TabIndex = 7
        Me.lbl_fin_points_j1.Text = "0"
        '
        'lbl_fin_j1
        '
        Me.lbl_fin_j1.AutoSize = True
        Me.lbl_fin_j1.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin_j1.Font = New System.Drawing.Font("Snap ITC", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fin_j1.Location = New System.Drawing.Point(40, 33)
        Me.lbl_fin_j1.Name = "lbl_fin_j1"
        Me.lbl_fin_j1.Size = New System.Drawing.Size(152, 36)
        Me.lbl_fin_j1.TabIndex = 8
        Me.lbl_fin_j1.Text = "Joueur 1"
        '
        'pnl_j1
        '
        Me.pnl_j1.BackColor = System.Drawing.Color.Transparent
        Me.pnl_j1.Controls.Add(Me.lbl_fin_j1)
        Me.pnl_j1.Controls.Add(Me.lbl_fin_points_j1)
        Me.pnl_j1.Location = New System.Drawing.Point(119, 140)
        Me.pnl_j1.Name = "pnl_j1"
        Me.pnl_j1.Size = New System.Drawing.Size(245, 137)
        Me.pnl_j1.TabIndex = 9
        '
        'pnl_j2
        '
        Me.pnl_j2.BackColor = System.Drawing.Color.Transparent
        Me.pnl_j2.Controls.Add(Me.lbl_fin_j2)
        Me.pnl_j2.Controls.Add(Me.lbl_fin_points_j2)
        Me.pnl_j2.Location = New System.Drawing.Point(415, 130)
        Me.pnl_j2.Name = "pnl_j2"
        Me.pnl_j2.Size = New System.Drawing.Size(269, 147)
        Me.pnl_j2.TabIndex = 10
        '
        'pnl_j3
        '
        Me.pnl_j3.BackColor = System.Drawing.Color.Transparent
        Me.pnl_j3.Controls.Add(Me.lbl_fin_j3)
        Me.pnl_j3.Controls.Add(Me.lbl_fin_points_j3)
        Me.pnl_j3.Location = New System.Drawing.Point(119, 277)
        Me.pnl_j3.Name = "pnl_j3"
        Me.pnl_j3.Size = New System.Drawing.Size(244, 116)
        Me.pnl_j3.TabIndex = 11
        '
        'pnl_j4
        '
        Me.pnl_j4.BackColor = System.Drawing.Color.Transparent
        Me.pnl_j4.Controls.Add(Me.lbl_fin_j4)
        Me.pnl_j4.Controls.Add(Me.lbl_fin_points_j4)
        Me.pnl_j4.Location = New System.Drawing.Point(415, 276)
        Me.pnl_j4.Name = "pnl_j4"
        Me.pnl_j4.Size = New System.Drawing.Size(269, 120)
        Me.pnl_j4.TabIndex = 12
        '
        'frm_fin_de_partie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.VB_menu_lancer_partie.My.Resources.Resources.ww
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.pnl_j4)
        Me.Controls.Add(Me.pnl_j3)
        Me.Controls.Add(Me.pnl_j2)
        Me.Controls.Add(Me.pnl_j1)
        Me.Controls.Add(Me.lbl_fin_de_partie)
        Me.Name = "frm_fin_de_partie"
        Me.Text = "fin de partie"
        Me.pnl_j1.ResumeLayout(False)
        Me.pnl_j1.PerformLayout()
        Me.pnl_j2.ResumeLayout(False)
        Me.pnl_j2.PerformLayout()
        Me.pnl_j3.ResumeLayout(False)
        Me.pnl_j3.PerformLayout()
        Me.pnl_j4.ResumeLayout(False)
        Me.pnl_j4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_fin_de_partie As Label
    Friend WithEvents lbl_fin_points_j4 As Label
    Friend WithEvents lbl_fin_j4 As Label
    Friend WithEvents lbl_fin_points_j2 As Label
    Friend WithEvents lbl_fin_j2 As Label
    Friend WithEvents lbl_fin_points_j3 As Label
    Friend WithEvents lbl_fin_j3 As Label
    Friend WithEvents lbl_fin_points_j1 As Label
    Friend WithEvents lbl_fin_j1 As Label
    Friend WithEvents pnl_j1 As Panel
    Friend WithEvents pnl_j2 As Panel
    Friend WithEvents pnl_j3 As Panel
    Friend WithEvents pnl_j4 As Panel
End Class
