﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_choix_type_jeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_question_choix = New System.Windows.Forms.Label()
        Me.rdb_passer = New System.Windows.Forms.RadioButton()
        Me.rdb_echanger = New System.Windows.Forms.RadioButton()
        Me.rdb_placer = New System.Windows.Forms.RadioButton()
        Me.cmd_valider_choix = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lbl_question_choix
        '
        Me.lbl_question_choix.Font = New System.Drawing.Font("Snap ITC", 28.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_question_choix.Location = New System.Drawing.Point(42, 56)
        Me.lbl_question_choix.Name = "lbl_question_choix"
        Me.lbl_question_choix.Size = New System.Drawing.Size(698, 118)
        Me.lbl_question_choix.TabIndex = 3
        Me.lbl_question_choix.Text = "Que voulez-vous faire ?"
        '
        'rdb_passer
        '
        Me.rdb_passer.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdb_passer.Font = New System.Drawing.Font("Snap ITC", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdb_passer.Location = New System.Drawing.Point(565, 177)
        Me.rdb_passer.Name = "rdb_passer"
        Me.rdb_passer.Size = New System.Drawing.Size(175, 87)
        Me.rdb_passer.TabIndex = 1
        Me.rdb_passer.TabStop = True
        Me.rdb_passer.Text = "Passer son tour"
        Me.rdb_passer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rdb_passer.UseVisualStyleBackColor = True
        '
        'rdb_echanger
        '
        Me.rdb_echanger.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdb_echanger.Font = New System.Drawing.Font("Snap ITC", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdb_echanger.Location = New System.Drawing.Point(307, 177)
        Me.rdb_echanger.Name = "rdb_echanger"
        Me.rdb_echanger.Size = New System.Drawing.Size(175, 87)
        Me.rdb_echanger.TabIndex = 2
        Me.rdb_echanger.TabStop = True
        Me.rdb_echanger.Text = "Echanger des tuiles"
        Me.rdb_echanger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rdb_echanger.UseVisualStyleBackColor = True
        '
        'rdb_placer
        '
        Me.rdb_placer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rdb_placer.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdb_placer.Font = New System.Drawing.Font("Snap ITC", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdb_placer.Location = New System.Drawing.Point(53, 177)
        Me.rdb_placer.Name = "rdb_placer"
        Me.rdb_placer.Size = New System.Drawing.Size(175, 87)
        Me.rdb_placer.TabIndex = 0
        Me.rdb_placer.TabStop = True
        Me.rdb_placer.Text = "Placer une tuile"
        Me.rdb_placer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rdb_placer.UseVisualStyleBackColor = True
        '
        'cmd_valider_choix
        '
        Me.cmd_valider_choix.Font = New System.Drawing.Font("Snap ITC", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_valider_choix.Location = New System.Drawing.Point(53, 320)
        Me.cmd_valider_choix.Name = "cmd_valider_choix"
        Me.cmd_valider_choix.Size = New System.Drawing.Size(687, 93)
        Me.cmd_valider_choix.TabIndex = 4
        Me.cmd_valider_choix.Text = "valider"
        Me.cmd_valider_choix.UseVisualStyleBackColor = True
        '
        'frm_choix_type_jeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.cmd_valider_choix)
        Me.Controls.Add(Me.lbl_question_choix)
        Me.Controls.Add(Me.rdb_echanger)
        Me.Controls.Add(Me.rdb_passer)
        Me.Controls.Add(Me.rdb_placer)
        Me.Name = "frm_choix_type_jeu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Qwirkle"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lbl_question_choix As Label
    Friend WithEvents rdb_passer As RadioButton
    Friend WithEvents rdb_echanger As RadioButton
    Friend WithEvents rdb_placer As RadioButton
    Friend WithEvents cmd_valider_choix As Button
End Class
