﻿Imports QwirkleLib


Public Class frm_tableau


    Dim Case1(position_x, position_y) As [Case]
    Dim tuilesJoueur() As Tuile
    Dim Pioche1 As New Pioche()
    Dim le_joueur As Integer
    Dim mongrp As GroupBox
    Dim choix As New Form
    Dim nombre_de_joueur As Integer

    ' initialisations des scores 
    Dim initscore1 As Integer
    Dim initscore2 As Integer
    Dim initscore3 As Integer
    Dim initscore4 As Integer

    Dim indicetuileplacer As Integer

    ' initialisation des joueurs
    Dim Joueur1 As New Joueur(My.Forms.Frm_lancer_partie.txt_j1.Text, initscore1 = 0, 0)
    Dim Joueur2 As New Joueur(My.Forms.Frm_lancer_partie.txt_j2.Text, initscore2 = 0, 0)
    Dim Joueur3 As New Joueur(My.Forms.Frm_lancer_partie.txt_j3.Text, initscore3 = 0, 0)
    Dim Joueur4 As New Joueur(My.Forms.Frm_lancer_partie.txt_j4.Text, initscore4 = 0, 0)
    Dim JOUEURS(3) As Joueur

    ' initialisation de la grille de jeu
    Dim grille(30, 30) As PictureBox
    Dim columns As Integer = 0
    Dim rows As Integer = 0
    Dim position_x As Integer = 100
    Dim position_y As Integer = 50
    Dim taille As System.Drawing.Size
    Dim compteur As Integer




    Private Sub Form_tableau_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        ' création de la grille
        taille.Height = 20
        taille.Width = 20

        Dim lieu As Point
        While rows < 30
            While columns < 30
                grille(columns, rows) = New PictureBox
                grille(columns, rows).BorderStyle = BorderStyle.FixedSingle
                grille(columns, rows).SizeMode = PictureBoxSizeMode.StretchImage
                grille(columns, rows).Visible = True
                grille(columns, rows).AllowDrop = True
                grille(columns, rows).Size = taille
                lieu.X = position_x
                lieu.Y = position_y
                grille(columns, rows).Location = lieu

                Me.Controls.Add(grille(columns, rows))

                AddHandler grille(columns, rows).DragEnter, AddressOf PictureBox1_DragEnter
                AddHandler grille(columns, rows).DragDrop, AddressOf PictureBox1_DragDrop
                AddHandler grille(columns, rows).MouseMove, AddressOf PictureBox1_MouseMove


                position_x += 20
                If position_x = 700 Then
                    position_x = 100
                    position_y += 20
                End If
                columns += 1
            End While
            rows += 1
            columns = 0




        End While


        grp_box_nom_j1.Text = My.Forms.Frm_lancer_partie.txt_j1.Text
        grp_box_nom_j2.Text = My.Forms.Frm_lancer_partie.txt_j2.Text
        grp_box_nom_j3.Text = My.Forms.Frm_lancer_partie.txt_j3.Text
        grp_box_nom_j4.Text = My.Forms.Frm_lancer_partie.txt_j4.Text

        ' initialisation du joueur 1
        Joueur1.SetName(My.Forms.Frm_lancer_partie.txt_j1.Text)
        Joueur1.SetAge(Val(My.Forms.Frm_lancer_partie.txt_age_j1.Text))
        Joueur1.SetMain_joueur(Pioche1.FirstDistributionTuiles())
        Joueur1.SetScore(0)
        initscore1 = lbl_pts_j1.Text

        ' initialisation du joueur 2
        Joueur2.SetName(My.Forms.Frm_lancer_partie.txt_j2.Text)
        Joueur2.SetAge(Val(My.Forms.Frm_lancer_partie.txt_age_j2.Text))
        Joueur2.SetMain_joueur(Pioche1.FirstDistributionTuiles())
        Joueur2.SetScore(0)
        initscore2 = lbl_pts_j2.Text

        ' initialisation du joueur 3
        Joueur3.SetName(My.Forms.Frm_lancer_partie.txt_j3.Text)
        Joueur3.SetAge(Val(My.Forms.Frm_lancer_partie.txt_age_j3.Text))
        Joueur3.SetMain_joueur(Pioche1.FirstDistributionTuiles())
        Joueur3.SetScore(0)
        initscore3 = lbl_pts_j3.Text

        ' initialisation du joueur 4
        Joueur4.SetName(My.Forms.Frm_lancer_partie.txt_j4.Text)
        Joueur4.SetAge(Val(My.Forms.Frm_lancer_partie.txt_age_j4.Text))
        Joueur4.SetMain_joueur(Pioche1.FirstDistributionTuiles())
        Joueur4.SetScore(0)
        initscore4 = lbl_pts_j4.Text

        If My.Forms.Frm_lancer_partie.rdb_2_joueurs.Checked = True Then
            grp_box_J3.Visible = False
            grp_box_J4.Visible = False
        Else
            If My.Forms.Frm_lancer_partie.rdb_3_joueurs.Checked = True Then
                grp_box_J3.Visible = True
                grp_box_J4.Visible = False

            End If
        End If

        grp_box_J2.Enabled = False
        grp_box_J3.Enabled = False
        grp_box_J4.Enabled = False

        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized

        ' suppression des joueurs en trop pour la distribution
        If My.Forms.Frm_lancer_partie.rdb_2_joueurs.Checked = True Then
            JOUEURS(0) = Joueur1
            JOUEURS(1) = Joueur2
            JOUEURS(2) = Nothing
            JOUEURS(3) = Nothing
            nombre_de_joueur = 2
        End If

        If My.Forms.Frm_lancer_partie.rdb_3_joueurs.Checked = True Then
            JOUEURS(0) = Joueur1
            JOUEURS(1) = Joueur2
            JOUEURS(2) = Joueur3
            JOUEURS(3) = Nothing
            nombre_de_joueur = 3

        End If

        If My.Forms.Frm_lancer_partie.rdb_4_joueurs.Checked = True Then
            JOUEURS(0) = Joueur1
            JOUEURS(1) = Joueur2
            JOUEURS(2) = Joueur3
            JOUEURS(3) = Joueur4
            nombre_de_joueur = 4

        End If


        '  1er distribution
        Dim numero_groupe_box As Integer
        Dim numero_picture_box As Integer
        numero_groupe_box = 0
        numero_picture_box = 0

        For numero_groupe_box = 1 To nombre_de_joueur


            mongrp = Me.Controls("grp_box_J" & numero_groupe_box.ToString).Controls("grp_box_pieces_j" & numero_groupe_box.ToString)
            Dim tuilesJoueur() As Tuile = JOUEURS(numero_groupe_box - 1).GetMain_joueur

            For numero_picture_box = 1 To 6
                Dim pic As PictureBox
                Dim nomtuile As String

                pic = mongrp.Controls("pic_piece_" & numero_groupe_box.ToString & numero_picture_box.ToString)
                nomtuile = tuilesJoueur(numero_picture_box - 1).GetForme & tuilesJoueur(numero_picture_box - 1).GetCouleur
                pic.Image = My.Resources.ResourceManager.GetObject(nomtuile)
                pic.Visible = True

            Next
        Next

        ' choix du 1er joueur
        If My.Forms.Frm_lancer_partie.rdb_4_joueurs.Checked = True Then
            If JOUEURS(0).DefinirPremierJoueur(Joueur1.GetMain_joueur) > JOUEURS(1).DefinirPremierJoueur(Joueur2.GetMain_joueur) And JOUEURS(0).DefinirPremierJoueur(Joueur1.GetMain_joueur) > JOUEURS(2).DefinirPremierJoueur(Joueur3.GetMain_joueur) And JOUEURS(0).DefinirPremierJoueur(Joueur1.GetMain_joueur) > JOUEURS(3).DefinirPremierJoueur(Joueur4.GetMain_joueur) Then
                grp_box_J1.Enabled = True
                grp_box_J2.Enabled = False
                grp_box_J3.Enabled = False
                grp_box_J4.Enabled = False

            ElseIf JOUEURS(1).DefinirPremierJoueur(Joueur2.GetMain_joueur) > JOUEURS(2).DefinirPremierJoueur(Joueur3.GetMain_joueur) And JOUEURS(1).DefinirPremierJoueur(Joueur2.GetMain_joueur) > JOUEURS(3).DefinirPremierJoueur(Joueur4.GetMain_joueur) Then
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = True
                grp_box_J3.Enabled = False
                grp_box_J4.Enabled = False

            ElseIf JOUEURS(2).DefinirPremierJoueur(Joueur3.GetMain_joueur) > JOUEURS(3).DefinirPremierJoueur(Joueur4.GetMain_joueur) Then
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = False
                grp_box_J3.Enabled = True
                grp_box_J4.Enabled = False

            Else
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = False
                grp_box_J3.Enabled = False
                grp_box_J4.Enabled = True

            End If
        ElseIf My.Forms.Frm_lancer_partie.rdb_3_joueurs.Checked = True Then

            If JOUEURS(0).DefinirPremierJoueur(Joueur1.GetMain_joueur) > JOUEURS(1).DefinirPremierJoueur(Joueur2.GetMain_joueur) And JOUEURS(0).DefinirPremierJoueur(Joueur1.GetMain_joueur) > JOUEURS(2).DefinirPremierJoueur(Joueur3.GetMain_joueur) Then
                grp_box_J1.Enabled = True
                grp_box_J2.Enabled = False
                grp_box_J3.Enabled = False

            ElseIf JOUEURS(1).DefinirPremierJoueur(Joueur2.GetMain_joueur) > JOUEURS(2).DefinirPremierJoueur(Joueur3.GetMain_joueur) Then
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = True
                grp_box_J3.Enabled = False


            Else
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = False
                grp_box_J3.Enabled = True
            End If
        ElseIf My.Forms.Frm_lancer_partie.rdb_2_joueurs.Checked = True Then

            If JOUEURS(0).DefinirPremierJoueur(Joueur1.GetMain_joueur) > JOUEURS(1).DefinirPremierJoueur(Joueur2.GetMain_joueur) Then
                grp_box_J1.Enabled = True
                grp_box_J2.Enabled = False



            Else
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = True

            End If




        End If

        ' choix du type de jeu
        compteur = 0
        choix = frm_choix_type_jeu
        choix.ShowDialog()

        pnl_echanger.Enabled = True
        pic_echange.Visible = True
        cmd_passer.Enabled = True


        If My.Forms.frm_choix_type_jeu.rdb_placer.Checked = True Then

            pnl_echanger.Enabled = False
            pic_echange.Visible = False
            cmd_passer.Enabled = False

        ElseIf My.Forms.frm_choix_type_jeu.rdb_echanger.Checked = True Then

            cmd_passer.Enabled = False
            'grille.Enabled = False

        ElseIf My.Forms.frm_choix_type_jeu.rdb_passer.Checked = True Then
            pnl_echanger.Enabled = False
            pic_echange.Visible = False
            'grille.Enabled = False
        End If

        pic_echange.AllowDrop = True


    End Sub



    Private Sub PictureBox1_MouseMove(sender As Object, e As MouseEventArgs) Handles pic_piece_11.MouseMove, pic_piece_16.MouseMove, pic_piece_15.MouseMove, pic_piece_14.MouseMove, pic_piece_13.MouseMove, pic_piece_12.MouseMove, pic_piece_46.MouseMove, pic_piece_36.MouseMove, pic_piece_26.MouseMove, pic_piece_45.MouseMove, pic_piece_35.MouseMove, pic_piece_25.MouseMove, pic_piece_44.MouseMove, pic_piece_34.MouseMove, pic_piece_24.MouseMove, pic_piece_43.MouseMove, pic_piece_33.MouseMove, pic_piece_23.MouseMove, pic_piece_42.MouseMove, pic_piece_32.MouseMove, pic_piece_22.MouseMove, pic_piece_41.MouseMove, pic_piece_31.MouseMove, pic_piece_21.MouseMove, pic_echange.MouseMove
        If e.Button = MouseButtons.Left And sender.Image IsNot Nothing Then
            sender.AllowDrop = False
            Dim rep As DragDropEffects
            Select Case sender.Name
                Case "pic_piece_11", "pic_piece_21", "pic_piece_31", "pic_piece_41"
                    indicetuileplacer = 0
                Case "pic_piece_12", "pic_piece_22", "pic_piece_32", "pic_piece_42"
                    indicetuileplacer = 1
                Case "pic_piece_13", "pic_piece_23", "pic_piece_33", "pic_piece_43"
                    indicetuileplacer = 2
                Case "pic_piece_14", "pic_piece_24", "pic_piece_34", "pic_piece_44"
                    indicetuileplacer = 3
                Case "pic_piece_15", "pic_piece_25", "pic_piece_35", "pic_piece_45"
                    indicetuileplacer = 4
                Case "pic_piece_16", "pic_piece_26", "pic_piece_36", "pic_piece_46"
                    indicetuileplacer = 5
            End Select
            rep = DoDragDrop(sender.Image, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                sender.AllowDrop = True
            End If

            sender.Image = Nothing


        End If



    End Sub

    Private Sub PictureBox1_DragDrop(sender As Object, e As DragEventArgs) Handles pic_piece_11.DragDrop, pic_piece_16.DragDrop, pic_piece_15.DragDrop, pic_piece_14.DragDrop, pic_piece_13.DragDrop, pic_piece_12.DragDrop, pic_piece_46.DragDrop, pic_piece_36.DragDrop, pic_piece_26.DragDrop, pic_piece_45.DragDrop, pic_piece_35.DragDrop, pic_piece_25.DragDrop, pic_piece_44.DragDrop, pic_piece_34.DragDrop, pic_piece_24.DragDrop, pic_piece_43.DragDrop, pic_piece_33.DragDrop, pic_piece_23.DragDrop, pic_piece_42.DragDrop, pic_piece_32.DragDrop, pic_piece_22.DragDrop, pic_piece_41.DragDrop, pic_piece_31.DragDrop, pic_piece_21.DragDrop, pic_echange.DragDrop

        Dim pic As PictureBox = sender
        pic.Image = e.Data.GetData(DataFormats.Bitmap)
        tuilesJoueur = JOUEURS(le_joueur).GetMain_joueur
        Dim placement As New [Case]((pic.Location.Y - 100) / 20, (pic.Location.X - 50) / 20)
        ' Dim placement_echange As New [Case]("181", "10")


        ' calcul pour les indice de la grille
        placement.SetCoord_x((pic.Location.X - 100) / 20)
        placement.SetCoord_y((pic.Location.Y - 50) / 20)

        ' position de la case echange
        'placement_echange.SetCoord_x("181")
        'placement_echange.SetCoord_y("10")

        placement.SetContenu(tuilesJoueur(indicetuileplacer))


        placement.GetContenu()

        Plateau.PlacementTuile(placement.GetContenu, placement.GetCoord_x, placement.GetCoord_y)
        'Plateau.PlacementTuile(placement_echange.GetContenu, placement_echange.GetCoord_x, placement.GetCoord_y)
        placement.VerifierCombi(compteur, placement.GetCoord_x, placement.GetCoord_y)

        'comptage des points
        If grp_box_J1.Enabled = True Then

            Dim points_final_j1 = Convert.ToInt32(lbl_pts_j1.Text) + Joueur1.ActualiserPoint(placement.GetCoord_x, placement.GetCoord_y)
            lbl_pts_j1.Text = points_final_j1.ToString
        End If
        If grp_box_J2.Enabled = True Then
            Dim points_final_j2 = Convert.ToInt32(lbl_pts_j2.Text) + Joueur2.ActualiserPoint(placement.GetCoord_x, placement.GetCoord_y)
            lbl_pts_j2.Text = points_final_j2.ToString
        End If
        If grp_box_J3.Enabled = True Then
            Dim points_final_j3 = Convert.ToInt32(lbl_pts_j3.Text) + Joueur3.ActualiserPoint(placement.GetCoord_x, placement.GetCoord_y)
            lbl_pts_j3.Text = points_final_j3.ToString
        End If
        If grp_box_J4.Enabled = True Then
            Dim points_final_j4 = Convert.ToInt32(lbl_pts_j4.Text) + Joueur4.ActualiserPoint(placement.GetCoord_x, placement.GetCoord_y)
            lbl_pts_j4.Text = points_final_j4.ToString
        End If




    End Sub

    Private Sub PictureBox1_DragEnter(sender As Object, e As DragEventArgs) Handles pic_piece_11.DragEnter, pic_piece_16.DragEnter, pic_piece_15.DragEnter, pic_piece_14.DragEnter, pic_piece_13.DragEnter, pic_piece_12.DragEnter, pic_piece_46.DragEnter, pic_piece_36.DragEnter, pic_piece_26.DragEnter, pic_piece_45.DragEnter, pic_piece_35.DragEnter, pic_piece_25.DragEnter, pic_piece_44.DragEnter, pic_piece_34.DragEnter, pic_piece_24.DragEnter, pic_piece_43.DragEnter, pic_piece_33.DragEnter, pic_piece_23.DragEnter, pic_piece_42.DragEnter, pic_piece_32.DragEnter, pic_piece_22.DragEnter, pic_piece_41.DragEnter, pic_piece_31.DragEnter, pic_piece_21.DragEnter, pic_echange.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub cmd_quitter_Click(sender As Object, e As EventArgs) Handles cmd_quitter.Click
        'fin de partie
        MessageBox.Show("Voulez vous vraiment arrêter la partie ?", "Quitter ""Qwirkle.""", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        If MsgBoxResult.Ok Then
            Dim frm_fin = frm_fin_de_partie
            frm_fin.ShowDialog()
            Me.Close()

        End If
    End Sub
    Private Sub cmd_valider_Click(sender As Object, e As EventArgs) Handles cmd_valider.Click
        compteur = 1

        'changement de joueur
        If My.Forms.Frm_lancer_partie.rdb_2_joueurs.Checked = True Then

            If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False Then
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = True
                le_joueur = 1
            Else
                If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True Then
                    grp_box_J1.Enabled = True
                    grp_box_J2.Enabled = False
                    le_joueur = 0
                End If

            End If

        ElseIf My.Forms.Frm_lancer_partie.rdb_3_joueurs.Checked = True Then

            If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False Then
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = True
                grp_box_J3.Enabled = False
                le_joueur = 1
            Else
                If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True And grp_box_J3.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = False
                    grp_box_J3.Enabled = True
                    le_joueur = 2
                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = True Then
                        grp_box_J1.Enabled = True
                        grp_box_J2.Enabled = False
                        grp_box_J3.Enabled = False
                        le_joueur = 0
                    End If
                End If

            End If

        ElseIf My.Forms.Frm_lancer_partie.rdb_4_joueurs.Checked = True Then


            If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False And grp_box_J4.Enabled = False Then
                grp_box_J1.Enabled = False
                grp_box_J2.Enabled = True
                grp_box_J3.Enabled = False
                grp_box_J4.Enabled = False
                le_joueur = 1
            Else
                If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True And grp_box_J3.Enabled = False And grp_box_J4.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = False
                    grp_box_J3.Enabled = True
                    grp_box_J4.Enabled = False
                    le_joueur = 2
                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = True And grp_box_J4.Enabled = False Then
                        grp_box_J1.Enabled = False
                        grp_box_J2.Enabled = False
                        grp_box_J3.Enabled = False
                        grp_box_J4.Enabled = True
                        le_joueur = 3
                    Else
                        If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False And grp_box_J4.Enabled = True Then
                            grp_box_J1.Enabled = True
                            grp_box_J2.Enabled = False
                            grp_box_J3.Enabled = False
                            grp_box_J4.Enabled = False
                            le_joueur = 0
                        End If
                    End If
                End If
            End If
        End If

        ' choix du type de jeu apres chaque tour
        choix = frm_choix_type_jeu
        choix.ShowDialog()

        pnl_echanger.Enabled = True
        pic_echange.Visible = True
        cmd_passer.Enabled = True
        'grille.Enabled = True

        If My.Forms.frm_choix_type_jeu.rdb_placer.Checked = True Then

            pnl_echanger.Enabled = False
            cmd_passer.Enabled = False
            pic_echange.Visible = False

        ElseIf My.Forms.frm_choix_type_jeu.rdb_echanger.Checked = True Then

            cmd_passer.Enabled = False
            'grille.Enabled = False

        ElseIf My.Forms.frm_choix_type_jeu.rdb_passer.Checked = True Then
            pnl_echanger.Enabled = False
            pic_echange.Visible = False
            'grille.Enabled = False
        End If

        ' distribution des tuiles
        Dim numero_groupe_box As Integer
        Dim numero_picture_box As Integer
        Dim distribution_tuile As Integer
        numero_groupe_box = 0
        numero_picture_box = 0
        distribution_tuile = 0

        For numero_groupe_box = 1 To nombre_de_joueur
            mongrp = Me.Controls("grp_box_J" & numero_groupe_box.ToString).Controls("grp_box_pieces_j" & numero_groupe_box.ToString)

            Dim tuilesJoueur() As Tuile = JOUEURS(numero_groupe_box - 1).GetMain_joueur
            For numero_picture_box = 1 To 6
                Dim pic As PictureBox
                pic = mongrp.Controls("pic_piece_" & numero_groupe_box.ToString & numero_picture_box.ToString)

                If pic.Image Is Nothing Then
                    For distribution_tuile = 0 To 5
                        tuilesJoueur(numero_picture_box - 1) = Nothing
                        Pioche1.DistributionTuiles(JOUEURS(numero_groupe_box - 1).GetMain_joueur)

                    Next

                End If
                pic.Image = My.Resources.ResourceManager.GetObject(tuilesJoueur(numero_picture_box - 1).GetForme + tuilesJoueur(numero_picture_box - 1).GetCouleur)
            Next

        Next

    End Sub

    Private Sub cmd_passer_Click(sender As Object, e As EventArgs) Handles cmd_passer.Click
        MessageBox.Show("Voulez vous vraiment passer ?", "Passer ""Qwirkle.""",
      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        If MsgBoxResult.Ok Then

            If My.Forms.Frm_lancer_partie.rdb_2_joueurs.Checked = True Then
                If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = True
                    le_joueur = 1
                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True Then
                        grp_box_J1.Enabled = True
                        grp_box_J2.Enabled = False
                        le_joueur = 0
                    End If

                End If

            ElseIf My.Forms.Frm_lancer_partie.rdb_3_joueurs.Checked = True Then

                If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = True
                    grp_box_J3.Enabled = False
                    le_joueur = 1
                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True And grp_box_J3.Enabled = False Then
                        grp_box_J1.Enabled = False
                        grp_box_J2.Enabled = False
                        grp_box_J3.Enabled = True
                        le_joueur = 2
                    Else
                        If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = True Then
                            grp_box_J1.Enabled = True
                            grp_box_J2.Enabled = False
                            grp_box_J3.Enabled = False
                            le_joueur = 0
                        End If
                    End If

                End If

            ElseIf My.Forms.Frm_lancer_partie.rdb_4_joueurs.Checked = True Then


                If grp_box_J1.Enabled = True And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False And grp_box_J4.Enabled = False Then
                    grp_box_J1.Enabled = False
                    grp_box_J2.Enabled = True
                    grp_box_J3.Enabled = False
                    grp_box_J4.Enabled = False
                    le_joueur = 1
                Else
                    If grp_box_J1.Enabled = False And grp_box_J2.Enabled = True And grp_box_J3.Enabled = False And grp_box_J4.Enabled = False Then
                        grp_box_J1.Enabled = False
                        grp_box_J2.Enabled = False
                        grp_box_J3.Enabled = True
                        grp_box_J4.Enabled = False
                        le_joueur = 2
                    Else
                        If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = True And grp_box_J4.Enabled = False Then
                            grp_box_J1.Enabled = False
                            grp_box_J2.Enabled = False
                            grp_box_J3.Enabled = False
                            grp_box_J4.Enabled = True
                            le_joueur = 3
                        Else
                            If grp_box_J1.Enabled = False And grp_box_J2.Enabled = False And grp_box_J3.Enabled = False And grp_box_J4.Enabled = True Then
                                grp_box_J1.Enabled = True
                                grp_box_J2.Enabled = False
                                grp_box_J3.Enabled = False
                                grp_box_J4.Enabled = False
                                le_joueur = 0
                            End If
                        End If
                    End If
                End If
            End If
        End If

        choix = frm_choix_type_jeu
        choix.ShowDialog()

        pnl_echanger.Enabled = True
        pic_echange.Visible = True
        cmd_passer.Enabled = True
        'grille.Enabled = True

        If My.Forms.frm_choix_type_jeu.rdb_placer.Checked = True Then

            pnl_echanger.Enabled = False
            pic_echange.Visible = False
            cmd_passer.Enabled = False

        ElseIf My.Forms.frm_choix_type_jeu.rdb_echanger.Checked = True Then

            cmd_passer.Enabled = False
            'grille.Enabled = False

        ElseIf My.Forms.frm_choix_type_jeu.rdb_passer.Checked = True Then
            pnl_echanger.Enabled = False
            pic_echange.Visible = False
            'grille.Enabled = False
        End If

    End Sub

    Private Sub cmd_aide_Click(sender As Object, e As EventArgs) Handles cmd_aide.Click
        System.Diagnostics.Process.Start("https://www.iello.fr/regles/Qwirkle_regles_FR_web.pdf")
    End Sub

    Private Sub cmb_echanger_Click(sender As Object, e As EventArgs) Handles cmd_echanger.Click
        pic_echange.Image = Nothing
    End Sub
End Class


